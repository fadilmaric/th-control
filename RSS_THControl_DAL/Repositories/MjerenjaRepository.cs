﻿using Microsoft.EntityFrameworkCore;
using RSS_THControl_DAL.Contexts;
using RSS_THControl_DAL.Contexts.DTO;
using RSS_THControl_DAL.Repositories.IRepositories;
using RSS_THControl_DB.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSS_THControl_DAL.Repositories
{
    public class MjerenjaRepository:IMjerenjaRepository
    {
        protected Context Context { get; }

        public MjerenjaRepository(Context context)
        {
            Context = context;
        }

        public List<MjerenjaVM> GetAll(DateTime datumOd, DateTime datumDo,int logerID,int grupaID,int podrgupaID)
        {
            return Context.MjerenjaVM.FromSql("GetMjerenja @p0,@p1,@p2,@p3,@p4",datumOd,datumDo,logerID,grupaID,podrgupaID).ToList();
        }
        public List<KriticnaMjerenjaVM> GetAllCritical()
        {
            var mjerenja = Context.KriticnaMjerenjaVM.FromSql("GetKriticnaMjerenja").ToList();
            foreach (var item in mjerenja)
            {
                if (item.T > item.Tmax)
                    item.ValidTMax = false;
                if (item.T < item.Tmin)
                    item.ValidTMin = false;
                if (item.H > item.Hmax)
                    item.ValidHMax = false;
                if (item.H < item.Hmin)
                    item.ValidHMin = false;
            }
            return mjerenja;
        }
        public List<ChartLineDijagram> GetDetaljeZaLoger(int logerID)
        {
            List<Mjerenja> mjerenja = Context.Mjerenja.FromSql("GetDetaljeMjerenjaZaLoger @p0", logerID).ToList();
            List<Mjerenja> mjerenja2 = new List<Mjerenja>();
          
            for (int i = 0; i < mjerenja.Count; i++)
            {
                if (i % 3 == 0)
                    mjerenja2.Add(mjerenja[i]);
            }
            List<ChartLineDijagram> glavnaLista = new List<ChartLineDijagram>();
            List<ChartLineDijagramPomocno> pomocnaLista1 = new List<ChartLineDijagramPomocno>();
            List<ChartLineDijagramPomocno> pomocnaLista2 = new List<ChartLineDijagramPomocno>();
            List<ChartLineDijagramPomocno> pomocnaLista3 = new List<ChartLineDijagramPomocno>();
            List<ChartLineDijagramPomocno> pomocnaLista4 = new List<ChartLineDijagramPomocno>();
            List<ChartLineDijagramPomocno> pomocnaLista5 = new List<ChartLineDijagramPomocno>();
            List<ChartLineDijagramPomocno> pomocnaLista6 = new List<ChartLineDijagramPomocno>();
            foreach (var item in mjerenja2)
            {
                ChartLineDijagramPomocno cp1 = new ChartLineDijagramPomocno();
                cp1.name = item.Vrijeme.ToString();
                cp1.value = item.T;
                pomocnaLista1.Add(cp1);
                ChartLineDijagramPomocno cp2 = new ChartLineDijagramPomocno();
                cp2.name = item.Vrijeme.ToString();
                cp2.value = item.Tmin;
                pomocnaLista2.Add(cp2);
                ChartLineDijagramPomocno cp3 = new ChartLineDijagramPomocno();
                cp3.name = item.Vrijeme.ToString();
                cp3.value = item.Tmax;
                pomocnaLista3.Add(cp3);
                ChartLineDijagramPomocno cp4 = new ChartLineDijagramPomocno();
                cp4.name = item.Vrijeme.ToString();
                cp4.value = item.H;
                pomocnaLista4.Add(cp4);
                ChartLineDijagramPomocno cp5 = new ChartLineDijagramPomocno();
                cp5.name = item.Vrijeme.ToString();
                cp5.value = item.Hmin;
                pomocnaLista5.Add(cp5);
                ChartLineDijagramPomocno cp6 = new ChartLineDijagramPomocno();
                cp6.name = item.Vrijeme.ToString();
                cp6.value = item.Hmax;
                pomocnaLista6.Add(cp6);
            }
            for (int i = 0; i < 6; i++)
            {
                ChartLineDijagram cl = new ChartLineDijagram();
                cl.series = new List<ChartLineDijagramPomocno>();
                if (i==0)
                {
                    cl.name = "T";
                    cl.series = pomocnaLista1;
                }
                if (i == 1)
                {
                    cl.name = "MinT";
                    cl.series = pomocnaLista2;
                }
                if (i == 2)
                {
                    cl.name = "MaxT";
                    cl.series = pomocnaLista3;
                }
                if (i == 3)
                {
                    cl.name = "H";
                    cl.series = pomocnaLista4;
                }
                if (i == 4)
                {
                    cl.name = "MinH";
                    cl.series = pomocnaLista5;
                }
                if (i == 5)
                {
                    cl.name = "MaxH";
                    cl.series = pomocnaLista6;
                }
                glavnaLista.Add(cl);
            }
            
           

            return glavnaLista;
        }
    }
}
