﻿using Microsoft.EntityFrameworkCore.Storage;
using RSS_THControl_DAL.Contexts;
using RSS_THControl_DAL.Repositories.IRepositories;
using System;
using System.Collections.Generic;
using System.Text;

namespace RSS_THControl_DAL.Repositories
{
    public abstract class Repository<TEntity, TPk> : IRepository<TEntity, TPk> where TEntity : class
    {
        protected Context Context { get; }

        protected Repository(Context context)
        {
            Context = context;
        }

        public int SaveChanges() => Context.SaveChanges();

        public virtual void Add(TEntity entity)
        {
            Context.Set<TEntity>().Add(entity);
        }

        public virtual void AddRange(IEnumerable<TEntity> entities)
        {
            Context.Set<TEntity>().AddRange(entities);
        }

        public virtual void Remove(TEntity entity, bool softDelete = true)
        {
            Context.Set<TEntity>().Remove(entity);
        }

        public virtual void RemoveRange(IEnumerable<TEntity> entities, bool softDelete = true)
        {
            Context.Set<TEntity>().RemoveRange(entities);
        }

        public virtual void Update(TEntity entity)
        {
            Context.Set<TEntity>().Update(entity);
        }

        public IDbContextTransaction BeginTransaction()
        {
            return Context.Database.BeginTransaction();
        }
    }

}
