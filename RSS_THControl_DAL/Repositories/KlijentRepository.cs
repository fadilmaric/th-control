﻿using Microsoft.EntityFrameworkCore;
using RSS_THControl_DAL.Contexts;
using RSS_THControl_DAL.Contexts.DTO;
using RSS_THControl_DAL.Repositories.IRepositories;
using RSS_THControl_DB.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSS_THControl_DAL.Repositories
{
    public class KlijentRepository:IKlijentRepository
    {
        protected Context Context { get; }

        public KlijentRepository(Context context)
        {
            Context = context;
        }

        public List<Klijenti> GetAll()
        {
            return Context.Klijenti.ToList();
        }
        public bool Insert(Klijenti k)
        {
            var kl = Context.Klijenti.Where(x => x.Naziv == k.Naziv).FirstOrDefault();
            if (kl == null)
            {
                Context.Klijenti.Add(k);
                Context.SaveChanges();
                return true;
            }
            else
                return false;
        }
        public bool Edit(Klijenti k)
        {
            var kl = Context.Klijenti.Where(x => x.Naziv == k.Naziv).FirstOrDefault();
            if (kl == null)
            {
                Context.Entry(k).State = EntityState.Modified;
                Context.SaveChanges();
                return true;
            }
            else
                return false;
        }
        public bool ChangeStatus(Klijenti k)
        {
            Klijenti kor = Context.Klijenti.Where(x => x.Id == k.Id).FirstOrDefault();
            kor.Active = !kor.Active;
            Context.SaveChanges();
            return true;
        }
        public List<KlijentiVM> CountLogeraPoKlijentu()
        {
            return Context.KlijentiVM.FromSql("CountLogeraPoKlijentu").ToList();
        }
    }
}
