﻿using Microsoft.EntityFrameworkCore;
using RSS_THControl_DAL.Contexts;
using RSS_THControl_DAL.Repositories.IRepositories;
using RSS_THControl_DB.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSS_THControl_DAL.Repositories
{
    public class GrupeRepository: IGrupeRepository
    {
        protected Context Context { get; }

        public GrupeRepository(Context context)
        {
            Context = context;
        }

        public List<Grupa> GetAll(int klijentID)
        {
            return Context.Grupa.Where(x=>x.KlijentId==klijentID || klijentID==0).ToList();
        }
        public bool Insert(Grupa k)
        {
            Context.Grupa.Add(k);
            Context.SaveChanges();
            return true;
        }
        public bool Edit(Grupa k)
        {
            Context.Entry(k).State = EntityState.Modified;
            Context.SaveChanges();
            return true;
        }
    }
}
