﻿using Microsoft.EntityFrameworkCore;
using RSS_THControl_DAL.Contexts;
using RSS_THControl_DAL.Contexts.DTO;
using RSS_THControl_DAL.Repositories.IRepositories;
using RSS_THControl_DB.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSS_THControl_DAL.Repositories
{
    public class PodgrupeRepository: IPodgrupeRepository
    {
        protected Context Context { get; }

        public PodgrupeRepository(Context context)
        {
            Context = context;
        }

        public List<PodGrupaVM> GetAll(int klijentID)
        {
            return Context.PodGrupaVM.FromSql("GetPodgrupe @p0", klijentID).ToList();
        }
        public bool Insert(PodGrupa k)
        {
            Context.PodGrupa.Add(k);
            Context.SaveChanges();
            return true;
        }
        public bool Edit(PodGrupa k)
        {
            Context.Entry(k).State = EntityState.Modified;
            Context.SaveChanges();
            return true;
        }
    }
}
