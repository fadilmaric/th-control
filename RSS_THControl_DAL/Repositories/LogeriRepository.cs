﻿using Microsoft.EntityFrameworkCore;
using RSS_THControl.Helpers;
using RSS_THControl_DAL.Contexts;
using RSS_THControl_DAL.Contexts.DTO;
using RSS_THControl_DAL.Repositories.IRepositories;
using RSS_THControl_DB.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSS_THControl_DAL.Repositories
{
    public class LogeriRepository : ILogeriRepository
    {
        protected Context Context { get; }

        public LogeriRepository(Context context)
        {
            Context = context;
        }

        public List<LogeriVM> GetAll(int klijentID, int grupaID, int podgrupaID)
        {
            List<LogeriVM> logeri = Context.LogeriVM.FromSql("GetLogere @p0,@p1,@p2", klijentID, grupaID, podgrupaID).ToList();
            List<KorisniciLogeriVM> korisnici = Context.KorisniciLogeriVM.FromSql("GetKorisnikeLogere @p0", klijentID).ToList();

            foreach (var item in logeri)
            {
                item.korisnici = new List<LogeriKorisniciVM>();
                foreach (var k in korisnici)
                {
                    LogeriKorisniciVM lg = new LogeriKorisniciVM();
                    lg.Id = k.Id;
                    lg.Ime = k.ImePrezime;
                    if (item.Id == k.IDLogera)
                        lg.Checked = true;
                    else
                        lg.Checked = false;

                    if (item.korisnici.Where(x => x.Id == k.Id).FirstOrDefault() == null)
                        item.korisnici.Add(lg);
                    else
                    {
                        if (lg.Checked)
                            item.korisnici.Where(x => x.Id == k.Id).FirstOrDefault().Checked = true;
                    }
                }

            }
            return logeri;
        }
        public List<LogeriVM> GetAllByUser(int korisnikID)
        {
            return Context.LogeriVM.FromSql("GetLogereKorisnika @p0", korisnikID).ToList();
        }
        public bool Insert(Logeri k)
        {
            Context.Logeri.Add(k);
            Context.SaveChanges();
            return true;
        }
        public bool Edit(Logeri k)
        {
            Context.Entry(k).State = EntityState.Modified;
            Context.SaveChanges();
            return true;
        }
        public bool EditLogeriKorisnici(LogeriVM k)
        {
            var loger = Context.Logeri.Where(x => x.Id == k.Id).FirstOrDefault();
            loger.Grupaid = k.Grupaid;
            loger.Email1 = k.Email1;
            loger.Email2 = k.Email2;
            loger.Hmax = k.Hmax;
            loger.Hmin = k.Hmin;
            loger.Idklijenta = k.Idklijenta;
            loger.Idposlovnice = k.Idposlovnice;
            loger.Naziv = k.Naziv;
            loger.Podgrupaid = k.Podgrupaid;
            loger.Tmax = k.Tmax;
            loger.Tmin = k.Tmin;
            Context.KorisniciLogeri.RemoveRange(Context.KorisniciLogeri.Where(x => x.Idlogera == loger.Id));
            foreach (var item in k.korisnici)
            {
                if (item.Checked)
                {
                    KorisniciLogeri kl = new KorisniciLogeri();
                    kl.Idklijenta = loger.Idklijenta;
                    kl.Idlogera = loger.Id;
                    kl.Idkorisnika = item.Id;
                    Context.KorisniciLogeri.Add(kl);
                    Context.SaveChanges();
                }

            }
            Context.SaveChanges();
            return true;
        }
        public bool ArduinoData(int IDUredjaja, string temp, string Hum)
        {
            Logeri l = Context.Logeri.Where(x => x.Id == IDUredjaja).FirstOrDefault();

            if (l != null)
            {
                Mjerenja m1 = Context.Mjerenja.Where(x => x.Idlogera == IDUredjaja).LastOrDefault();
                Mjerenja m = new Mjerenja();
                m.Idlogera = l.Id;
                m.Hmax = l.Hmax;
                m.Hmin = l.Hmin;
                m.Tmax = l.Tmax;
                m.Tmin = l.Tmin;
                m.Vrijeme = DateTime.Now;
                m.T = Convert.ToDecimal(temp);
                m.H = Convert.ToDecimal(Hum);
                Context.Mjerenja.Add(m);
                Context.SaveChanges();
                  if (m1 != null)
                  {
                      if (Convert.ToDecimal(m1.T) > l.Tmin && Convert.ToDecimal(m1.T) < l.Tmax && Convert.ToDecimal(m1.H) > l.Hmin && Convert.ToDecimal(m1.H) < l.Hmax)
                      {
                          if (Convert.ToDecimal(temp) < l.Tmin || Convert.ToDecimal(temp) > l.Tmax)
                          {
                              EmailHelper.PosaljiMail("Temperatura uredjaja sa ID:" + l.Id + " nije u granicama!! " + "Zadana:" + l.Tmin + "-" + l.Tmax + ", Trenutna: " + temp + ".", "TH-Control", l.Email1);
                              EmailHelper.PosaljiMail("Temperatura uredjaja sa ID:" + l.Id + " nije u granicama!! " + "Zadana:" + l.Tmin + "-" + l.Tmax + ", Trenutna: " + temp + ".", "TH-Control", l.Email2);

                          }

                          if (Convert.ToDecimal(Hum) < l.Hmin || Convert.ToDecimal(Hum) > l.Hmax)
                          {
                              EmailHelper.PosaljiMail("Vlaznost uredjaja sa ID:" + l.Id + " nije u granicama!! " + "Zadana:" + l.Hmin + "-" + l.Hmax + ", Trenutna: " + Hum + ".", "TH-Control", l.Email1);
                              EmailHelper.PosaljiMail("Vlaznost uredjaja sa ID:" + l.Id + " nije u granicama!! " + "Zadana:" + l.Hmin + "-" + l.Hmax + ", Trenutna: " + Hum + ".", "TH-Control", l.Email2);

                          }
                      }
                  }
                  else
                  {
                      if (Convert.ToDecimal(temp) < l.Tmin || Convert.ToDecimal(temp) > l.Tmax)
                      {
                          EmailHelper.PosaljiMail("Temperatura uredjaja sa ID:" + l.Id + " nije u granicama!! " + "Zadana:" + l.Tmin + "-" + l.Tmax + ", Trenutna: " + temp + ".", "TH-Control", l.Email1);
                          EmailHelper.PosaljiMail("Temperatura uredjaja sa ID:" + l.Id + " nije u granicama!! " + "Zadana:" + l.Tmin + "-" + l.Tmax + ", Trenutna: " + temp + ".", "TH-Control", l.Email2);

                      }

                      if (Convert.ToDecimal(Hum) < l.Hmin || Convert.ToDecimal(Hum) > l.Hmax)
                      {
                          EmailHelper.PosaljiMail("Vlaznost uredjaja sa ID:" + l.Id + " nije u granicama!! " + "Zadana:" + l.Hmin + "-" + l.Hmax + ", Trenutna: " + Hum + ".", "TH-Control", l.Email1);
                          EmailHelper.PosaljiMail("Vlaznost uredjaja sa ID:" + l.Id + " nije u granicama!! " + "Zadana:" + l.Hmin + "-" + l.Hmax + ", Trenutna: " + Hum + ".", "TH-Control", l.Email2);

                      }
                  }
           //     EmailHelper.PosaljiMail("Temperatura uredjaja sa ID:" + l.Id + " nije u granicama!! " + "Zadana:" + l.Tmin + "-" + l.Tmax + ", Trenutna: " + temp + ".", "TH-Control", "thtestthtest@gmail.com");


                return true;
            }
            else
                return false;



        }
        public List<LogeriMjerenjaVM> ProvjeraMjerenja(int klijentID, int grupaID, int podgrupaID)
        {
            List<LogeriMjerenjaVM> logeri = Context.LogeriMjerenjaVM.FromSql("ProvjeraMjerenja @p0,@p1,@p2", klijentID, grupaID, podgrupaID).ToList();
            foreach (var item in logeri)
            {
                if (item.VrijemeMjerenja > DateTime.Now.AddHours(-12))
                    item.Valid = true;
            }
            return logeri;
        }
        public List<LogeriTStanjeVM> TrenutnoStanje(int klijentID)
        {
            List<LogeriTStanjeVM> logeri = Context.LogeriTStanjeVM.FromSql("LogeriTrenutnoStanje @p0", klijentID).ToList();
            foreach (var item in logeri)
            {
                if (item.T < item.Tmin || item.T > item.Tmax)
                    item.ValidT = false;
                if (item.H < item.Hmin || item.H > item.Hmax)
                    item.ValidH = false;

            }
            return logeri;
        }
        public List<LogeriTStanjeVM> TrenutnoStanjeZaKorisnika(int korisnikID)
        {
            List<LogeriTStanjeVM> logeri = Context.LogeriTStanjeVM.FromSql("LogeriTrenutnoStanjeZaKorisnika @p0", korisnikID).ToList();
            foreach (var item in logeri)
            {
                if (item.T < item.Tmin || item.T > item.Tmax)
                    item.ValidT = false;
                if (item.H < item.Hmin || item.H > item.Hmax)
                    item.ValidH = false;

            }
            return logeri;
        }
    }
}
