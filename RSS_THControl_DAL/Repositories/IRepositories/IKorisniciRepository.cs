﻿using System;
using System.Collections.Generic;
using System.Text;
using RSS_THControl_DAL.Contexts.DTO;
using RSS_THControl_DB.Models;


namespace RSS_THControl_DAL.Repositories.IRepositories
{
    public interface IKorisniciRepository
    {
        List<KorisniciVM> GetAll(int ulogaID,int klijentID);
        bool Insert(Korisnici k);
        bool Edit(Korisnici k);
        bool ChangeStatus(Korisnici k);
        Korisnici Login(string username,string password);
    }

}
