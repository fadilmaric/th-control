﻿using RSS_THControl_DB.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace RSS_THControl_DAL.Repositories.IRepositories
{
    public interface IGrupeRepository
    {
        List<Grupa> GetAll(int klijentID);
        bool Insert(Grupa k);
        bool Edit(Grupa k);
    }
}
