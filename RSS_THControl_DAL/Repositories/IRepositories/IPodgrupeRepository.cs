﻿using RSS_THControl_DAL.Contexts.DTO;
using RSS_THControl_DB.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace RSS_THControl_DAL.Repositories.IRepositories
{
    public interface IPodgrupeRepository
    {
        List<PodGrupaVM> GetAll(int klijentID);
        bool Insert(PodGrupa k);
        bool Edit(PodGrupa k);
    }
}
