﻿using RSS_THControl_DAL.Contexts.DTO;
using RSS_THControl_DB.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace RSS_THControl_DAL.Repositories.IRepositories
{
    public interface IKlijentRepository
    {
        List<Klijenti> GetAll();
        bool Insert(Klijenti k);
        bool Edit(Klijenti k);
        bool ChangeStatus(Klijenti k);
        List<KlijentiVM> CountLogeraPoKlijentu();
    }
}
