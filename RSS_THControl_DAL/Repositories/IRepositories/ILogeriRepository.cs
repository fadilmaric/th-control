﻿using RSS_THControl_DAL.Contexts.DTO;
using RSS_THControl_DB.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace RSS_THControl_DAL.Repositories.IRepositories
{
    public interface ILogeriRepository
    {
        List<LogeriVM> GetAll(int klijentID, int grupaID, int podgrupaID);
        List<LogeriVM> GetAllByUser(int korisnikID);
        bool Insert(Logeri l);
        bool Edit(Logeri k);
        bool EditLogeriKorisnici(LogeriVM k);
        bool ArduinoData(int IDUredjaja, string temp, string Hum);
        List<LogeriMjerenjaVM> ProvjeraMjerenja(int klijentID, int grupaID, int podgrupaID);
        List<LogeriTStanjeVM> TrenutnoStanje(int klijentID);
        List<LogeriTStanjeVM> TrenutnoStanjeZaKorisnika(int korisnikID);
    }
    
}
