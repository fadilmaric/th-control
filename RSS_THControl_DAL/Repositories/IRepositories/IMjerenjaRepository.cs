﻿using RSS_THControl_DAL.Contexts.DTO;
using RSS_THControl_DB.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace RSS_THControl_DAL.Repositories.IRepositories
{
    public interface IMjerenjaRepository
    {
        List<MjerenjaVM> GetAll(DateTime datumOd, DateTime datumDo, int logerID,int grupaID,int podgrupaID);
        List<KriticnaMjerenjaVM> GetAllCritical();
        List<ChartLineDijagram> GetDetaljeZaLoger(int logerID);
    }
}
