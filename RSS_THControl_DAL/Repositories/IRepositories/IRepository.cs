﻿using Microsoft.EntityFrameworkCore.Storage;
using System;
using System.Collections.Generic;
using System.Text;

namespace RSS_THControl_DAL.Repositories.IRepositories
{
    public interface IRepository<TEntity, in TPk> where TEntity : class
    {
        int SaveChanges();
        void Add(TEntity entity);
        void AddRange(IEnumerable<TEntity> entities);
        void Remove(TEntity entity, bool softDelete = true);
        void RemoveRange(IEnumerable<TEntity> entities, bool softDelete = true);
        void Update(TEntity entity);
        IDbContextTransaction BeginTransaction();
    }

}
