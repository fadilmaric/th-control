﻿using Microsoft.EntityFrameworkCore;
using RSS_THControl_DAL.Contexts;
using RSS_THControl_DAL.Contexts.DTO;
using RSS_THControl_DAL.Repositories.IRepositories;
using RSS_THControl_DB.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace RSS_THControl_DAL.Repositories
{
    public class KorisniciRepository : IKorisniciRepository
    {
        protected Context Context { get; }

        public KorisniciRepository(Context context)
        {
            Context = context;
        }

        public List<KorisniciVM> GetAll(int ulogaID, int klijentID)
        {
            return Context.KorisniciVM.FromSql("GetKorisnike @p0,@p1", ulogaID, klijentID).ToList();
        }
        public static string EnkodirajMD5(string lozinka)
        {
            string enkLozinka = string.Empty;

            if (!string.IsNullOrEmpty(lozinka))
            {
                System.Byte[] originalBytes;
                System.Byte[] encodedBytes;
                MD5 md5;

                md5 = new MD5CryptoServiceProvider();
                originalBytes = System.Text.ASCIIEncoding.Default.GetBytes(lozinka);
                encodedBytes = md5.ComputeHash(originalBytes);
                enkLozinka = Convert.ToBase64String(encodedBytes);
            }

            return enkLozinka;
        }
        public Korisnici Login(string username, string password)
        {
            Korisnici k = Context.Korisnici.Where(x => x.Ime == username && x.Lozinka == EnkodirajMD5(password)).FirstOrDefault();
            if (k != null)
                return k;
            else
                return null;
        }
        public bool Insert(Korisnici k)
        {
            var kor = Context.Korisnici.Where(x => x.Ime == k.Ime).FirstOrDefault();
            if (kor == null)
            {
                k.Lozinka = EnkodirajMD5(k.Lozinka);
                Context.Korisnici.Add(k);
                Context.SaveChanges();
                return true;
            }
            else
                return false;

        }
        public bool Edit(Korisnici k)
        {
            List<KorisniciVM> korisnici= Context.KorisniciVM.FromSql("GetKorisnike @p0,@p1", 0, k.Firma).ToList();
            Korisnici kor = Context.Korisnici.Where(x => x.Ime == k.Ime && x.Id != k.Id).FirstOrDefault();

            if (kor == null)
            {
                if (!string.IsNullOrEmpty(k.Lozinka))
                    k.Lozinka = EnkodirajMD5(k.Lozinka);
                else
                {
                    var temp = korisnici.Where(x=>x.Id==k.Id).FirstOrDefault();
                    k.Lozinka = temp.Lozinka;
                }
                Context.Entry(k).State = EntityState.Modified;
                Context.SaveChanges();
                return true;
            }
            else
                return false;
        }

      
        public bool ChangeStatus(Korisnici k)
        {
            Korisnici kor = Context.Korisnici.Where(x => x.Id == k.Id).FirstOrDefault();
            kor.Active = !kor.Active;
            Context.SaveChanges();
            return true;
        }
    }
}
