﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;

namespace RSS_THControl.Helpers
{
    public static class EmailHelper
    {
        public static void PosaljiMail(string poruka, string subject, string mailTo)
        {
            try
            {
                string to = mailTo;
                string from = "thtestthtest@gmail.com";
                string body = poruka;
                MailMessage msg = new MailMessage(from, to, subject, body);

                SmtpClient smtpClient = new SmtpClient("smtp.gmail.com")//new SmtpClient("smtp.gmail.com")
                {
                    UseDefaultCredentials = false,
                    DeliveryMethod = SmtpDeliveryMethod.Network,
                    Credentials = new NetworkCredential(from, "thcontrol123"),
                    Port = 587,
                    EnableSsl = true,
                };

                smtpClient.Send(msg);
                msg.Dispose();
                smtpClient.Dispose();
            }
            catch (Exception exp)
            {
                Console.WriteLine(exp.ToString());
            }
        }
    }
}
