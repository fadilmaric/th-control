﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RSS_THControl_DAL.Contexts.DTO
{
    public class PodGrupaVM
    {
        public int Id { get; set; }
        public string Naziv { get; set; }
        public int? GrupaId { get; set; }
        public int? KlijentId { get; set; }
        public string Grupa { get; set; }
    }
}
