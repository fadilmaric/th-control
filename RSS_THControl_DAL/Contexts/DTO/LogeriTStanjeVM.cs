﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RSS_THControl_DAL.Contexts.DTO
{
    public class LogeriTStanjeVM
    {
        public int Id { get; set; }
        public string Naziv { get; set; }
        public int? Idklijenta { get; set; }
        public string Klijent { get; set; }
        public DateTime? VrijemeMjerenja { get; set; }
        public decimal? T { get; set; }
        public decimal? H { get; set; }
        public decimal? Tmin { get; set; }
        public decimal? Tmax { get; set; }
        public decimal? Hmin { get; set; }
        public decimal? Hmax { get; set; }
        public bool ValidT { get; set; }
        public bool ValidH { get; set; }
    }
}
