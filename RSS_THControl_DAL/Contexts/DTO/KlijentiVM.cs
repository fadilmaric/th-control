﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RSS_THControl_DAL.Contexts.DTO
{
    public class KlijentiVM
    {
        public int Id { get; set; }
        public string Naziv { get; set; }
        public int BrojLogera { get; set; }
    }
}
