﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RSS_THControl_DAL.Contexts.DTO
{
    public class KorisniciVM
    {
        public int Id { get; set; }
        public string Ime { get; set; }
        public string Lozinka { get; set; }
        public bool Status { get; set; }
        public string ImePrezime { get; set; }
        public int Firma { get; set; }
        public int Poslovnica { get; set; }
        public int UlogaID { get; set; }
        public bool Active { get; set; }
        public string Klijent { get; set; }
        public string Uloga { get; set; }
    }
}
