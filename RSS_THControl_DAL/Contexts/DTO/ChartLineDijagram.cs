﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RSS_THControl_DAL.Contexts.DTO
{
    public class ChartLineDijagram
    {
        public string name { get; set; }
        public List<ChartLineDijagramPomocno> series { get; set; }
    }
    public class ChartLineDijagramPomocno
    {
        public decimal? value { get; set; }
        public string name { get; set; }
    }
}
