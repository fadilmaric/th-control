﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RSS_THControl_DAL.Contexts.DTO
{
    public class KriticnaMjerenjaVM
    {
        public int Int { get; set; }
        public int? Idlogera { get; set; }
        public DateTime? Vrijeme { get; set; }
        public decimal? T { get; set; }
        public decimal? H { get; set; }
        public decimal? Tmin { get; set; }
        public decimal? Tmax { get; set; }
        public decimal? Hmin { get; set; }
        public decimal? Hmax { get; set; }
        public string Loger { get; set; }
        public bool ValidTMin { get; set; }
        public bool ValidTMax { get; set; }
        public bool ValidHMin { get; set; }
        public bool ValidHMax { get; set; }

    }
}
