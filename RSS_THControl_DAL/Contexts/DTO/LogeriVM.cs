﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RSS_THControl_DAL.Contexts.DTO
{
    public class LogeriVM
    {
        public int Id { get; set; }
        public string Naziv { get; set; }
        public int? Idklijenta { get; set; }
        public int? Idposlovnice { get; set; }
        public decimal Tmin { get; set; }
        public decimal? Tmax { get; set; }
        public decimal? Hmin { get; set; }
        public decimal? Hmax { get; set; }
        public string Email1 { get; set; }
        public string Email2 { get; set; }
        public string Klijent { get; set; }
        public int? Grupaid { get; set; }
        public int? Podgrupaid { get; set; }
        public List<LogeriKorisniciVM> korisnici { get; set; }
     
    }
    public class LogeriKorisniciVM
    {
        public int Id { get; set; }
        public string Ime { get; set; }
        public bool Checked { get; set; }
    }
}
