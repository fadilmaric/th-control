﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace RSS_THControl_DB.Models
{
    public partial class RSSDB_THControlContext : DbContext
    {
        public RSSDB_THControlContext()
        {
        }

        public RSSDB_THControlContext(DbContextOptions<RSSDB_THControlContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Grupa> Grupa { get; set; }
        public virtual DbSet<Klijenti> Klijenti { get; set; }
        public virtual DbSet<KorisniciLogeri> KorisniciLogeri { get; set; }
        public virtual DbSet<Logeri> Logeri { get; set; }
        public virtual DbSet<Mjerenja> Mjerenja { get; set; }
        public virtual DbSet<PodGrupa> PodGrupa { get; set; }
        public virtual DbSet<Poslovnice> Poslovnice { get; set; }
        public virtual DbSet<Uloge> Uloge { get; set; }

        // Unable to generate entity type for table 'dbo.Korisnici'. Please see the warning messages.

//        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
//        {
//            if (!optionsBuilder.IsConfigured)
//            {
//#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
//                optionsBuilder.UseSqlServer("Server=144.76.183.4,1433;Database=RSSDB_THControl;Trusted_Connection=False;User ID=adna;Password=adna2018?*");
//            }
//        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Grupa>(entity =>
            {
                entity.Property(e => e.Id);

                entity.Property(e => e.Naziv).HasMaxLength(100);
            });

            modelBuilder.Entity<Klijenti>(entity =>
            {
                entity.Property(e => e.Id)
                    .ValueGeneratedNever();

                entity.Property(e => e.Naziv).HasMaxLength(100);
            });

            modelBuilder.Entity<KorisniciLogeri>(entity =>
            {
                entity.Property(e => e.Id)
                    .ValueGeneratedNever();

                entity.Property(e => e.Idklijenta);

                entity.Property(e => e.Idkorisnika);

                entity.Property(e => e.Idlogera);
            });

            modelBuilder.Entity<Logeri>(entity =>
            {
                entity.Property(e => e.Id)
                    .ValueGeneratedNever();

                entity.Property(e => e.Email1)
                    .HasMaxLength(64);

                entity.Property(e => e.Email2)
                    .HasMaxLength(64);

                entity.Property(e => e.Hmax);

                entity.Property(e => e.Hmin);

                entity.Property(e => e.Idklijenta);

                entity.Property(e => e.Idposlovnice);

                entity.Property(e => e.Naziv).HasMaxLength(100);

                entity.Property(e => e.Tmax);

                entity.Property(e => e.Tmin);
            });

            modelBuilder.Entity<Mjerenja>(entity =>
            {
                entity.HasKey(e => e.Int);

                entity.Property(e => e.Int);

                entity.Property(e => e.Hmax);

                entity.Property(e => e.Hmin);

                entity.Property(e => e.Idlogera);

                entity.Property(e => e.Tmax);

                entity.Property(e => e.Tmin);

                entity.Property(e => e.Vrijeme);
            });

            modelBuilder.Entity<PodGrupa>(entity =>
            {
                entity.Property(e => e.Id);

                entity.Property(e => e.GrupaId);

                entity.Property(e => e.Naziv).HasMaxLength(100);
            });

            modelBuilder.Entity<Poslovnice>(entity =>
            {
                entity.Property(e => e.Id);

                entity.Property(e => e.Idklijenta);

                entity.Property(e => e.Naziv)
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Uloge>(entity =>
            {
                entity.Property(e => e.Id);

                entity.Property(e => e.Naziv).HasMaxLength(100);
            });
        }
    }
}
