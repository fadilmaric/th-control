﻿using System;
using System.Collections.Generic;

namespace RSS_THControl_DB.Models
{
    public partial class KorisniciLogeri
    {
        public int Id { get; set; }
        public int? Idklijenta { get; set; }
        public int? Idlogera { get; set; }
        public int? Idkorisnika { get; set; }
    }
}
