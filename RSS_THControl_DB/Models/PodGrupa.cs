﻿using System;
using System.Collections.Generic;

namespace RSS_THControl_DB.Models
{
    public partial class PodGrupa
    {
        public int Id { get; set; }
        public string Naziv { get; set; }
        public int? GrupaId { get; set; }
        public int? KlijentId { get; set; }
    }
}
