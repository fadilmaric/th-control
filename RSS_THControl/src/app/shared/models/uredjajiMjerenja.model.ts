export class UredjajiMjerenja{
    id:number;
    naziv:string;
    idklijenta:number;
    idposlovnice:number;
    tmin:number;
    tmax:number;
    hmin:number;
    hmax:number;
    email1:string;
    email2:string;
    klijent:string;
    grupaid:number;
    podgrupaid:number;
    korsnici:any[];
    vrijemeMjerenja:string;
    valid:boolean;
}