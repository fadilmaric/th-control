export class MjerenjaVM{
    id:number;
    idlogera:number;
    vrijeme:string;
    t:number;
    h:number;
    tmin:number;
    tmax:number;
    hmin:number;
    hmax:number;
    loger:string;
    validTMin:boolean;
    validTMax:boolean;
    validHMin:boolean;
    validHMax:boolean;
}