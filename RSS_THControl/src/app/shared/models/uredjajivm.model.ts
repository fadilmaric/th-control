export class UredjajiVM{
    id:number;
    h:number;
    hmax:number;
    hmin:number;
    idklijenta:number;
    klijent:string;
    naziv:string;
    t:number;
    tmax:number;
    tmin:number;
    validH:boolean;
    validT:boolean;
    vrijemeMjerenja:string;
}