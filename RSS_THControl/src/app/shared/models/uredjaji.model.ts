import { KorisniciUredjaji } from "./korisniciuredjaji.model";

export class Uredjaji{
    id:number;
    naziv:string;
    idklijenta:number;
    idposlovnice:number;
    tmin:number;
    tmax:number;
    hmin:number;
    hmax:number;
    email1:string;
    email2:String;
    klijent:string;
    grupaid:number;
    podgrupaid:number;
    korisnici:KorisniciUredjaji[];
}