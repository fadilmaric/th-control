export class Mjerenja{
    int:number;
    h:number;
    hmax:number;
    hmin:number;
    idlogera:number;
    loger:string;
    t:number;
    tmax:number;
    tmin:number;
    vrijeme:string;
}