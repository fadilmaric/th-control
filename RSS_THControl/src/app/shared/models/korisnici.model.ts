export class Korisnici{
    id:number;
    firma:number;
    ime:string;
    imePrezime:string;
    poslovnica:number;
    ulogaID:number;
    lozinka:string;
    klijent:string;
    uloga:string;
    active:boolean;
}