import { Injectable } from "@angular/core";
import { Router } from "../../../../node_modules/@angular/router";
import { Klijent } from "../models/klijent.model";
import { RestService } from "./rest.service";
import { Korisnici } from "../models/korisnici.model";
import { Uredjaji } from "../models/uredjaji.model";
import { Mjerenja } from "../models/mjerenja.model";
import { Grupa } from "../models/grupa.model";
import { Podgrupa } from "../models/podgrupa.model";
import { KlijentVM } from "../models/klijentvm.model";
import { UredjajiMjerenja } from "../models/uredjajiMjerenja.model";
import { MjerenjaVM } from "../models/mjerenjavm.model";
import { UredjajiVM } from "../models/uredjajivm.model";

@Injectable()
export class DataService {

    public colors: string[] = ["#716aca", "#36a3f7", "#f4516c", "#34bfa3", "B2B1CF", "#F1BC2F"];
    public userRole: number;
    public klijenti: Klijent[];
    public korisnici: Korisnici[];
    public uredjaji: Uredjaji[];
    public selectedKlijent: Klijent;
    public selectedKorisnik: Korisnici;
    public selectedUredjaj: Uredjaji;
    public selectedGrupa: Grupa;
    public selectedPodgrupa: Podgrupa;
    public loggedUser: Korisnici;
    public username: string;
    public imePrezime: string;
    public korisnikId: number;
    public zaposlenici: Korisnici[];
    public mjerenja: Mjerenja[];
    public klijentId: number;
    public grupe: Grupa[];
    public podgrupe: Podgrupa[];
    public filteredPodgrupe: Podgrupa[] = [];
    public brojLogera: KlijentVM[];
    public uredjajiValid: UredjajiMjerenja[];
    public criticalMjerenja:MjerenjaVM[];
    public trenutnoStanje: UredjajiVM[];
    public trenutnoStanjeKorisnik: UredjajiVM[];

    constructor(private router: Router, public _service: RestService) {
    }

    authorization() {
        this.userRole = parseInt(sessionStorage.getItem("Role"));
        this.klijentId = parseInt(sessionStorage.getItem("KlijentId"));
        this.username = sessionStorage.getItem("Username");
        this.imePrezime = sessionStorage.getItem("ImePrezime");
        this.korisnikId = parseInt(sessionStorage.getItem("KorisnikId"));
        if (this.userRole == null) {
            this.router.navigate(["/prijava"]);
        }
        else if (this.userRole == 3) {
            this.router.navigate(["/rss"]);
        }
        else if (this.userRole == 1) {
            this.router.navigate(["/admin"]);
        }
        else if (this.userRole == 2) {
            this.router.navigate(["/zaposlenik"]);
        }
    }

    getKlijenti() {
        this._service.getKlijenti().subscribe((data: any) => {
            this.klijenti = data;
        });
    }

    getKorisnike() {
        if (this.klijentId == undefined || this.klijentId == null)
            this.klijentId = 0;
        this._service.getKorisnike(this.userRole, this.klijentId).subscribe((data: any) => {
            this.korisnici = data;
        })
    }

    getZaposlenike() {
        if (this.klijentId == undefined || this.klijentId == null)
            this.klijentId = 0;
        this._service.getKorisnike(this.userRole, this.klijentId).subscribe((data: any) => {
            this.zaposlenici = data;
        })
    }

    getUredjaje(grupaId, podgrupaId) {
        if (this.klijentId == undefined || this.klijentId == null)
            this.klijentId = 0;
        this._service.getUredjaje(this.klijentId, grupaId, podgrupaId).subscribe((data: any) => {
            this.uredjaji = data;
        });
    }

    getUredjajeByKorisnik(korisnikId) {
        this._service.getUredjajeByKorisnik(korisnikId).subscribe((data: any) => {
            this.uredjaji = data;
        });
    }

    getGrupe() {
        if (this.klijentId == undefined || this.klijentId == null)
            this.klijentId = 0;
        this._service.getGrupe(this.klijentId).subscribe((data: any) => {
            this.grupe = data;
        });
    }

    getPodgrupe() {
        if (this.klijentId == undefined || this.klijentId == null)
            this.klijentId = 0;
        this._service.getPodgrupe(this.klijentId).subscribe((data: any) => {
            this.podgrupe = data;
        });
    }

    getPodgrupeByGroup(groupId) {
        this.filteredPodgrupe = [];
        if (this.klijentId == undefined || this.klijentId == null)
            this.klijentId = 0;
        this._service.getPodgrupe(this.klijentId).subscribe((data: any) => {
            this.podgrupe = data;
            for (var i = 0; i < this.podgrupe.length; i++) {
                if (this.podgrupe[i].grupaId == groupId)
                    this.filteredPodgrupe.push(this.podgrupe[i]);
            }
        });
    }

    getBrojLogera() {
        this._service.getCountUredjaja().subscribe((data: any) => {
            this.brojLogera = data;
            var n = 0;
            for (var i = 0; i < this.brojLogera.length; i++) {
                if (n < 6) {
                    this.brojLogera[i].color = this.colors[n];
                    n++;
                }
                else
                    n = 0;
            }
        });
    }


    getTrenutnoStanje(){
        this._service.getTrenutnoStanje(this.klijentId).subscribe((data: any) => {
            this.trenutnoStanje = data;
        });
    }


    getTrenutnoStanjeKorisnik(){
        this._service.getTrenutnoStanjeKorisnik(this.korisnikId).subscribe((data: any) => {
            this.trenutnoStanjeKorisnik = data;
        });
    }

    checkMjerenja(grupaId, podgrupaId) {
        if (this.klijentId == undefined || this.klijentId == null)
            this.klijentId = 0;
        this._service.checkMjerenja(this.klijentId,grupaId, podgrupaId).subscribe((data: any) => {
            this.uredjajiValid = data;
        });
    }


    getAllCritical(){
        this._service.getAllCritical().subscribe((data: any) => {
            this.criticalMjerenja = data;
            console.log(this.criticalMjerenja);
        });
    }

    logout() {
        sessionStorage.removeItem("Role");
        sessionStorage.removeItem("Username");
        this.router.navigate(["/prijava"]);
    }
}