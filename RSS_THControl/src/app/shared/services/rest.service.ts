import { Injectable } from "@angular/core";
import { Router } from "../../../../node_modules/@angular/router";
import { HttpClient } from "../../../../node_modules/@angular/common/http";

@Injectable()
export class RestService {

    public url_path = "http://th-control.ml/api/";

    constructor(private http: HttpClient) {
    }

    getKlijenti() {
        return this.http.get(this.url_path + "Klijenti/GetAll");
    }

    getKorisnike(ulogaid, klijentId) {
        return this.http.get(this.url_path + "Korisnici/GetAll?ulogaID=" + ulogaid + "&klijentID=" + klijentId);
    }

    getUredjaje(klijentId, grupaId, podgrupaId) {
        return this.http.get(this.url_path + "Logeri/GetAll?klijentID=" + klijentId+ "&grupaID="+grupaId+"&podgrupaId="+podgrupaId);
    }

    getUredjajeByKorisnik(korisnikId){
        return this.http.get(this.url_path + "Logeri/GetAllByUser?korisnikID=" + korisnikId);
    }

    getMjerenja(startDate, endDate, logerId, grupaId, podgrupaId) {
        return this.http.get(this.url_path + "Mjerenja/GetAll?datumOd=" + startDate + "&datumDo=" + endDate + "&logerID=" + logerId +
            "&grupaID=" + grupaId + "&podgrupaId=" + podgrupaId);
    }

    getDetaljiLogera(uredjajId){
        return this.http.get(this.url_path + "Mjerenja/GetDetaljeZaLoger?logerID=" + uredjajId);
    }

    getGrupe(klijentId) {
        return this.http.get(this.url_path + "Grupe/GetAll?klijentID=" + klijentId);
    }

    getPodgrupe(klijentId) {
        return this.http.get(this.url_path + "Podgrupe/GetAll?klijentID=" + klijentId);
    }

    getCountUredjaja(){
        return this.http.get(this.url_path + "Klijenti/CountLogeraPoKlijentu");
    }

    getAllCritical(){
        return this.http.get(this.url_path + "Mjerenja/getallcritical");
    }

    getTrenutnoStanje(klijentId){
        return this.http.get(this.url_path + "Logeri/trenutnostanje?klijentID=" + klijentId);
    }

    getTrenutnoStanjeKorisnik(korisnikId){
        return this.http.get(this.url_path + "Logeri/TrenutnoStanjeZaKorisnika?korisnikID=" + korisnikId);
    }

    checkMjerenja(klijentID, grupaId, podgrupaId){
        return this.http.get(this.url_path + "Logeri/provjeramjerenja?klijentID="+klijentID+"&grupaId="+grupaId+"&podgrupaId="+podgrupaId);
    }

    addKlijent(klijent) {
        return this.http.post(this.url_path + "Klijenti/Insert", klijent);
    }

    addKorisnik(korisnik) {
        return this.http.post(this.url_path + "Korisnici/Insert", korisnik);
    }

    addUredjaj(uredjaj) {
        return this.http.post(this.url_path + "Logeri/Insert", uredjaj);
    }

    addGrupa(grupa) {
        return this.http.post(this.url_path + "Grupe/Insert", grupa);
    }

    addPodgrupa(grupa) {
        return this.http.post(this.url_path + "Podgrupe/Insert", grupa);
    }

    updateKlijent(klijent) {
        return this.http.post(this.url_path + "Klijenti/Edit", klijent);
    }

    updateKorisnik(korisnik) {
        return this.http.post(this.url_path + "Korisnici/Edit", korisnik);
    }

    updateUredjaj(uredjaj) {
        return this.http.post(this.url_path + "Logeri/Edit", uredjaj);
    }

    updateGrupa(grupa) {
        return this.http.post(this.url_path + "Grupe/Edit", grupa);
    }

    updatePodgrupa(podgrupa) {
        return this.http.post(this.url_path + "Podgrupe/Edit", podgrupa);
    }

    updateKorisnikeLogere(uredjaj) {
        return this.http.post(this.url_path + "Logeri/EditLogeriKorisnici", uredjaj);
    }

    changeKlijentStatus(klijent) {
        return this.http.post(this.url_path + "Klijenti/ChangeStatus", klijent);
    }

    changeKorisnikStatus(korisnik) {
        return this.http.post(this.url_path + "Korisnici/ChangeStatus", korisnik);
    }

    login(username, password) {
        return this.http.get(this.url_path + "Korisnici/Login?username=" + username + "&password=" + password);
    }
}