export class Resource{
    public static get ERROR(): string { return "Dogodila se greška. Pokušajte ponovno!"; };
    public static get UPDATE_SUCCESS(): string { return "Uspješno ste ažurirali podatke!"; };
    public static get ADD_SUCCESS(): string { return "Uspješno ste dodali podatak!"; };
    public static get DELETE_SUCCESS(): string { return "Uspješno ste obrisali podatak!"; };
    public static get STATUS_SUCCESS(): string { return "Uspješno ste promijenili status!"; };
    public static get LOGIN_ERROR(): string { return "Niste unijeli ispravne korisničke podatke!"; };
    public static get USERNAME_ERROR(): string { return "Korisničko ime je zauzeto!"; };

}