import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { DataService } from '../../shared/services/data.service';
import { Korisnici } from '../../shared/models/korisnici.model';
import { Resource } from '../../shared/resource';
import { HttpErrorResponse } from '../../../../node_modules/@angular/common/http';
import { Validators, FormBuilder, FormGroup } from '../../../../node_modules/@angular/forms';
import { FlashMessagesService } from '../../../../node_modules/angular2-flash-messages';
import { RestService } from '../../shared/services/rest.service';
import { Klijent } from '../../shared/models/klijent.model';

@Component({
  selector: 'zaposlenik-profil',
  templateUrl: './profil.component.html'
})
export class ProfilZaposlenikComponent implements OnInit{

    public klijenti: Klijent[];
    public korisnik: Korisnici;
    public korisnikForm: FormGroup;
    public submitted: boolean = false;
  
    get f() { return this.korisnikForm.controls; }
  
    constructor(private _flashMessagesService: FlashMessagesService, private formBuilder: FormBuilder, public _service: RestService, 
      public _dataService:DataService, private _changeRef: ChangeDetectorRef) { }
  
    ngOnInit() {
      this.korisnikForm = this.formBuilder.group({
        imePrezime: [this._dataService.imePrezime, Validators.required],
        korisnickoIme: [this._dataService.username, Validators.required],
        lozinka: [''],
      });
    }
  
    submit() {
      if (this.korisnikForm.invalid) {
        this.submitted = true;
        return;
      }
  
      this.korisnik = new Korisnici();
      this.korisnik.id=this._dataService.korisnikId;
      this.korisnik.imePrezime = this.f.imePrezime.value;
      this.korisnik.ime = this.f.korisnickoIme.value;
      this.korisnik.firma = this._dataService.klijentId;
      this.korisnik.lozinka = this.f.lozinka.value;
      this.korisnik.ulogaID = 2;

      this._dataService.username=this.f.korisnickoIme.value;
      this._dataService.imePrezime=this.f.imePrezime.value;
      this._changeRef.detectChanges();
  
      this._service.updateKorisnik(this.korisnik).subscribe((data: any) => {
        if (data == true) {
          this._flashMessagesService.show(Resource.UPDATE_SUCCESS, { cssClass: 'alert-success', timeout: 5000 });
        }
        else {
          this._flashMessagesService.show(Resource.USERNAME_ERROR, { cssClass: 'alert-danger', timeout: 5000 });
        }
      }, (err: HttpErrorResponse) => {
        this._flashMessagesService.show(Resource.ERROR, { cssClass: 'alert-danger', timeout: 5000 });
      });
    }
}
