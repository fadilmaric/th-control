import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { DataService } from '../../shared/services/data.service';
import { RestService } from '../../shared/services/rest.service';

@Component({
  selector: 'zaposlenik-dashboard',
  templateUrl: './dashboard.component.html',
})
export class ZaposlenikDashboardComponent implements OnInit{

    showXAxis1 = true;
    showYAxis1 = true;
    gradient1 = false;
    showLegend1 = true;
    showXAxisLabel1 = false;
    xAxisLabel1 = '';
    showYAxisLabel1 = true;
    yAxisLabel1 = '';
    timeline1 = false;
    legendTitle1 = "";
    view = [500, 300];
    public isChart = false;

    multi1: any[] = [
        {
            name: 'Cyan',
            series: [
                {
                    name: 5,
                    value: 2650
                },
                {
                    name: 10,
                    value: 2800
                },
                {
                    name: 15,
                    value: 2000
                }
            ]
        },
        {
            name: 'Yellow',
            series: [
                {
                    name: 5,
                    value: 2500
                },
                {
                    name: 9,
                    value: 5100
                },
                {
                    name: 15,
                    value: 2350
                }
            ]
        }
    ];

    colorScheme1 = {
        domain: ['#6e339c', '#a099d5', '#75f65b', '#e9668b', '#ddf731', '#bcac18', '#3c4a74', '#2646f1', '#d62c73', '#f23d65', '#6e339c', '#6e339c', '#abaa9b', '#bcac18', '#579305']
    };

  constructor(public _service:DataService, public restService: RestService, private changeRef: ChangeDetectorRef){}

  ngOnInit(){
    this._service.getTrenutnoStanjeKorisnik();
  }

  openChart(uredjajId) {
    this.restService.getDetaljiLogera(uredjajId).subscribe((data: any) => {
        this.multi1 = data;
        this.isChart = true;
        this.changeRef.detectChanges();
    })
}
}
