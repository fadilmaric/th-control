import { Component, OnInit } from '@angular/core';
import { DataService } from '../../shared/services/data.service';
import { Validators, FormGroup, FormBuilder } from '../../../../node_modules/@angular/forms';
import { Uredjaji } from '../../shared/models/uredjaji.model';
import { FlashMessagesService } from '../../../../node_modules/angular2-flash-messages';
import { Router } from '../../../../node_modules/@angular/router';
import { NgxSpinnerService } from '../../../../node_modules/ngx-spinner';
import { RestService } from '../../shared/services/rest.service';

declare var $:any;

@Component({
  selector: 'zaposlenik-mjerenja',
  templateUrl: './mjerenja.component.html'
})
export class MjerenjaEmployeeComponent implements OnInit{

    public p: number = 1;
    public startDate: any;
    public endDate: any;
    public filterForm: FormGroup;
    public uredjaji: Uredjaji[];
    public selectedKlijent: number;

    get f() { return this.filterForm.controls; }

    constructor(public _service: DataService, private router: Router, private _restService: RestService,
        private _flashMessagesService: FlashMessagesService, private spinner: NgxSpinnerService,
        private formBuilder: FormBuilder) { }

    ngOnInit() {
        this._service.mjerenja=[];
        var start = new Date();
        this.startDate = (start.getMonth() + 1) + '/' + start.getDate() + '/' + start.getFullYear();
        this.endDate = (start.getMonth() + 1) + '/' + start.getDate() + '/' + start.getFullYear();
        this.selectedKlijent = -1;

        $(document).ready(function () {
            $("#m_datepicker_2").datepicker({ todayHighlight: !0, orientation: "bottom left" });
            $("#m_datepicker_3").datepicker({ todayHighlight: !0, orientation: "bottom left" });
        });

        this.filterForm = this.formBuilder.group({
            startDate: [this.startDate, Validators.required],
            endDate: [this.endDate, Validators.required],
            uredjajId: [-1, Validators.required],
        });

        if (this._service.klijentId == undefined || this._service.klijentId == null)
            this._service.klijentId = 0;
        this._restService.getUredjajeByKorisnik(this._service.korisnikId).subscribe((data: any) => {
            this.uredjaji = data;
        });

    }

    pretrazi(startDate, endDate) {
        var sDate = startDate.value;
        var eDate = endDate.value;
        var uredjajId = this.f.uredjajId.value;
        var grupaId = 0;
        var podgrupaId = 0;

        if (sDate == "" && eDate == "") {
            this.startDate = this.startDate;
            this.endDate = this.endDate;
        }

        if (sDate != "" || eDate != "") {
            if (sDate != "")
                this.startDate = sDate;
            if (eDate != "")
                this.endDate = eDate;
            if (sDate == "")
                this.startDate = this.endDate;
            else if (eDate == "")
                this.endDate = this.startDate;
        }

        if (uredjajId == -1)
            uredjajId = 0;

        this.spinner.show();
        this._restService.getMjerenja(this.startDate, this.endDate, uredjajId, grupaId, podgrupaId).subscribe((data: any) => {
            this._service.mjerenja = data;
            this.spinner.hide();
        });
    }
}
