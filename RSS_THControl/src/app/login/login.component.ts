import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '../../../node_modules/@angular/forms';
import { RestService } from '../shared/services/rest.service';
import { FlashMessagesService } from '../../../node_modules/angular2-flash-messages/module/flash-messages.service';
import { HttpErrorResponse } from '../../../node_modules/@angular/common/http';
import { Resource } from '../shared/resource';
import { DataService } from '../shared/services/data.service';

@Component({
  selector: 'login',
  templateUrl: './login.component.html',
})
export class LoginComponent implements OnInit{

  public loginForm: FormGroup;
  public submitted: boolean = false;

  get f() { return this.loginForm.controls; }

  constructor(private formBuilder: FormBuilder, public _service: RestService, private _flashMessagesService: FlashMessagesService, 
  private _dataService:DataService) { }

  ngOnInit(){
    sessionStorage.removeItem("KlijentId");
    this._dataService.klijentId=0;
    this.loginForm = this.formBuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.required],
    });
  }

  login(){
    if (this.loginForm.invalid) {
      this.submitted = true;
      return;
    }

    var username=this.f.username.value;
    var password=this.f.password.value;

    this._service.login(username, password).subscribe((data:any)=>{
      if(data==null){
        this._flashMessagesService.show(Resource.LOGIN_ERROR, { cssClass: 'alert-danger', timeout: 5000 });
      }
      else{
        this._dataService.loggedUser=data;
        sessionStorage.setItem("Username", data.ime);
        sessionStorage.setItem("Role", data.ulogaID);
        sessionStorage.setItem("KlijentId", data.firma);
        sessionStorage.setItem("ImePrezime", data.imePrezime);
        sessionStorage.setItem("KorisnikId", data.id);
        this._dataService.authorization();
      }
    }, (err: HttpErrorResponse) => {
      this._flashMessagesService.show(Resource.LOGIN_ERROR, { cssClass: 'alert-danger', timeout: 5000 });
    });
  }
}
