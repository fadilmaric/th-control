import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HttpErrorResponse } from '@angular/common/http';
import { DataService } from '../shared/services/data.service';

declare var $: any;

@Component({
  selector: 'super-admin',
  templateUrl: './super-administrator.component.html',
  styleUrls:['./super-administrator.component.css']
})

export class SuperAdminComponent implements OnInit {
  title = 'Vodovod';
  isOpenSidebar: boolean;
  isOpenToggle: boolean;

  constructor(private router: Router, public _service:DataService) {
    this.isOpenSidebar = false;
    this.isOpenToggle = false;
  }

  ngOnInit() {
  }

  logout() {
   this._service.logout();
  }

  openSidebar() {
    var self = this;
    if (!self.isOpenSidebar) {
      document.getElementById("m_aside_left").style.left = "0";
      document.getElementById("m_aside_left").style.visibility = "visible";
      $(document).ready(function () {
        $(".m-aside-left-close").css("left", "230px");
      })
      self.isOpenSidebar = true;
    }
    else {
      document.getElementById("m_aside_left").style.left = "-265px";
      document.getElementById("m_aside_left").style.visibility = "hidden";
      $(document).ready(function () {
        $(".m-aside-left-close").css("left", "-25px");
      })
      self.isOpenSidebar = false;
    }
  }

  openHeader() {
    var self = this;
    if (!self.isOpenToggle) {
      $("body").addClass("m-topbar--on")
      self.isOpenToggle = true;
    }
    else {
      $("body").removeClass("m-topbar--on")
      self.isOpenToggle = false;
    }
  }

  closeSidebar() {
    var self = this;
    document.getElementById("m_aside_left").style.left = "-265px";
    document.getElementById("m_aside_left").style.visibility = "hidden";
    $(document).ready(function () {
      $(".m-aside-left-close").css("left", "-25px");
    })
    self.isOpenSidebar = false;
  }

  onResize(event) {
    var self = this;
    if (event.target.innerWidth > 1024) {
      document.getElementById("m_aside_left").style.left = "0";
      document.getElementById("m_aside_left").style.visibility = "visible";
      $(document).ready(function () {
        $(".m-aside-left-close").css("left", "230px");
      })
      self.isOpenSidebar = true;
    }
    else {
      document.getElementById("m_aside_left").style.left = "-265px";
      document.getElementById("m_aside_left").style.visibility = "hidden";
      $(document).ready(function () {
        $(".m-aside-left-close").css("left", "-25px");
      })
      self.isOpenSidebar = false;
    }
  }
}
