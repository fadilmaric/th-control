import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '../../../../../node_modules/@angular/forms';
import { Klijent } from '../../../shared/models/klijent.model';
import { Korisnici } from '../../../shared/models/korisnici.model';
import { FlashMessagesService } from '../../../../../node_modules/angular2-flash-messages';
import { RestService } from '../../../shared/services/rest.service';
import { Uredjaji } from '../../../shared/models/uredjaji.model';
import { Resource } from '../../../shared/resource';
import { HttpErrorResponse } from '../../../../../node_modules/@angular/common/http';
import { DataService } from '../../../shared/services/data.service';

@Component({
    selector: 'edit-uredjaj',
    templateUrl: './edit.component.html',
})
export class UredjajEditComponent implements OnInit {

    public klijenti: Klijent[];
    public uredjaj: Uredjaji;
    public uredjajForm: FormGroup;
    public submitted: boolean = false;
    public selectedKlijent:number;

    get f() { return this.uredjajForm.controls; }

    constructor(private _flashMessagesService: FlashMessagesService, private formBuilder: FormBuilder, public _service: RestService,
        private _dataService: DataService) { }

    ngOnInit() {
        console.log(this._dataService.selectedUredjaj);
        this.uredjajForm = this.formBuilder.group({
            naziv: [this._dataService.selectedUredjaj.naziv, Validators.required],
            klijentId: [this._dataService.selectedUredjaj.idklijenta, Validators.required],
            email1: [this._dataService.selectedUredjaj.email1, Validators.required],
            email2: [this._dataService.selectedUredjaj.email2, Validators.required],
            tmin: [this._dataService.selectedUredjaj.tmin, Validators.required],
            tmax: [this._dataService.selectedUredjaj.tmax, Validators.required],
            hmin: [this._dataService.selectedUredjaj.hmin, Validators.required],
            hmax: [this._dataService.selectedUredjaj.hmax, Validators.required],
        });

        this._service.getKlijenti().subscribe((data: any) => {
            this.klijenti = data;
            this.uredjajForm.controls['klijentId'].setValue(this._dataService.selectedUredjaj.idklijenta);
        });
    }

    submit() {
        if (this.uredjajForm.invalid) {
            this.submitted = true;
            return;
        }

        this.uredjaj = this._dataService.selectedUredjaj;
        this.uredjaj.naziv = this.f.naziv.value;
        this.uredjaj.idklijenta = this.f.klijentId.value;
        this.uredjaj.email1 = this.f.email1.value;
        this.uredjaj.email2 = this.f.email2.value;
        this.uredjaj.tmin = parseFloat(this.f.tmin.value);
        this.uredjaj.tmax = parseFloat(this.f.tmax.value);
        this.uredjaj.hmax = parseFloat(this.f.hmax.value);
        this.uredjaj.hmin = parseFloat(this.f.hmin.value);

        this._service.updateUredjaj(this.uredjaj).subscribe((data: any) => {
            this._flashMessagesService.show(Resource.UPDATE_SUCCESS, { cssClass: 'alert-success', timeout: 5000 });
        }, (err: HttpErrorResponse) => {
            this._flashMessagesService.show(Resource.ERROR, { cssClass: 'alert-danger', timeout: 5000 });
        });
    }
}
