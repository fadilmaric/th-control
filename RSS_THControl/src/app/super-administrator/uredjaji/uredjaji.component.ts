import { Component, OnInit } from '@angular/core';
import { DataService } from '../../shared/services/data.service';
import { Router } from '../../../../node_modules/@angular/router';
import { RestService } from '../../shared/services/rest.service';
import { FlashMessagesService } from '../../../../node_modules/angular2-flash-messages';
import { Resource } from '../../shared/resource';

@Component({
    selector: 'uredjaj',
    templateUrl: './uredjaji.component.html',
})
export class UredjajComponent implements OnInit {

    public p = 1;
    constructor(public _service: DataService, private router: Router, private _restService: RestService, 
        private _flashMessagesService: FlashMessagesService) { }

    ngOnInit() {
        this._service.getUredjaje(0,0);
    }

    edit(uredjaj) {
        this._service.selectedUredjaj = uredjaj;
        this.router.navigate(['rss/uredjaji/uredi', this._service.selectedUredjaj.id]);
    }
}
