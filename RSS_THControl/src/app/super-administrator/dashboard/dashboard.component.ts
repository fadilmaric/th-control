import { Component, OnInit } from '@angular/core';
import { DataService } from '../../shared/services/data.service';

@Component({
    selector: 'super-dashboard',
    templateUrl: './dashboard.component.html',
})
export class SuperAdminDashboardComponent implements OnInit {

    public p=1;
    constructor(public _service: DataService) { }

    ngOnInit() {
        this._service.getBrojLogera();
        this._service.checkMjerenja(0,0);
    }
}
