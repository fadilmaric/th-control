import { Component, OnInit } from '@angular/core';
import { DataService } from '../../shared/services/data.service';
import { Router } from '../../../../node_modules/@angular/router';
import { RestService } from '../../shared/services/rest.service';
import { Resource } from '../../shared/resource';
import { FlashMessagesService } from '../../../../node_modules/angular2-flash-messages';

@Component({
  selector: 'klijent',
  templateUrl: './klijent.component.html',
})
export class KlijentComponent implements OnInit {

  public p: number = 1;

  constructor(public _service: DataService, private router:Router, private _restService: RestService,
    private _flashMessagesService: FlashMessagesService) { }

  ngOnInit() {
    this._service.getKlijenti();
  }

  edit(klijent) {
    this._service.selectedKlijent = klijent;
    this.router.navigate(['rss/klijenti/uredi', this._service.selectedKlijent.id]);
  }

  changeStatus(klijent){
    this._restService.changeKlijentStatus(klijent).subscribe((data:any)=>{
      this._flashMessagesService.show(Resource.STATUS_SUCCESS, { cssClass: 'alert-success', timeout: 5000 });
      this.ngOnInit();
    });
  }
}
