import { Component, OnInit } from '@angular/core';
import { Klijent } from '../../../shared/models/klijent.model';
import { FormGroup, FormBuilder, Validators } from '../../../../../node_modules/@angular/forms';
import { HttpErrorResponse } from '../../../../../node_modules/@angular/common/http';
import { DataService } from '../../../shared/services/data.service';
import { RestService } from '../../../shared/services/rest.service';
import { FlashMessagesService } from '../../../../../node_modules/angular2-flash-messages';
import { Resource } from '../../../shared/resource';

@Component({
  selector: 'add-klijent',
  templateUrl: './add.component.html',
})

export class KlijentAddComponent implements OnInit {

  public klijent: Klijent;
  public klijentForm: FormGroup;
  public submitted: boolean = false;

  get f() { return this.klijentForm.controls; }

  constructor(private formBuilder: FormBuilder, public _service: RestService, private _flashMessagesService: FlashMessagesService) { }

  ngOnInit() {
    this.klijentForm = this.formBuilder.group({
      naziv: ['', Validators.required],
    });
  }

  submit() {
    if (this.klijentForm.invalid) {
      this.submitted = true;
      return;
    }

    this.klijent = new Klijent();
    this.klijent.naziv = this.f.naziv.value;
    this.klijent.active=true;

    this._service.addKlijent(this.klijent).subscribe((data: any) => {
     this._flashMessagesService.show(Resource.ADD_SUCCESS, { cssClass: 'alert-success', timeout: 5000 });
      this.ngOnInit();
    }, (err: HttpErrorResponse) => {
      this._flashMessagesService.show(Resource.ERROR, { cssClass: 'alert-danger', timeout: 5000 });
    });
  }
}
