import { Component, OnInit } from '@angular/core';
import { Klijent } from '../../../shared/models/klijent.model';
import { FormGroup, FormBuilder, Validators } from '../../../../../node_modules/@angular/forms';
import { RestService } from '../../../shared/services/rest.service';
import { FlashMessagesService } from '../../../../../node_modules/angular2-flash-messages';
import { HttpErrorResponse } from '../../../../../node_modules/@angular/common/http';
import { Resource } from '../../../shared/resource';
import { DataService } from '../../../shared/services/data.service';

@Component({
  selector: 'edit-klijent',
  templateUrl: './edit.component.html',
})

export class KlijentEditComponent implements OnInit{

  public klijent: Klijent;
  public klijentForm: FormGroup;
  public submitted: boolean = false;

  get f() { return this.klijentForm.controls; }

  constructor(private formBuilder: FormBuilder, public _service: RestService, private _flashMessagesService: FlashMessagesService, 
  private _dataService:DataService) { }

  ngOnInit() {
    this.klijentForm = this.formBuilder.group({
      naziv: [this._dataService.selectedKlijent.naziv, Validators.required],
    });
  }

  submit() {
    if (this.klijentForm.invalid) {
      this.submitted = true;
      return;
    }

    this.klijent = new Klijent();
    this.klijent.id=this._dataService.selectedKlijent.id;
    this.klijent.active=this._dataService.selectedKlijent.active;
    this.klijent.naziv = this.f.naziv.value;

    this._service.updateKlijent(this.klijent).subscribe((data: any) => {
     this._flashMessagesService.show(Resource.UPDATE_SUCCESS, { cssClass: 'alert-success', timeout: 5000 });
    }, (err: HttpErrorResponse) => {
      this._flashMessagesService.show(Resource.ERROR, { cssClass: 'alert-danger', timeout: 5000 });
    });
  }
}
