import { Component, OnInit } from '@angular/core';
import { Korisnici } from '../../../shared/models/korisnici.model';
import { FormGroup, FormBuilder, Validators } from '../../../../../node_modules/@angular/forms';
import { RestService } from '../../../shared/services/rest.service';
import { FlashMessagesService } from '../../../../../node_modules/angular2-flash-messages';
import { Klijent } from '../../../shared/models/klijent.model';
import { Resource } from '../../../shared/resource';
import { HttpErrorResponse } from '../../../../../node_modules/@angular/common/http';

@Component({
  selector: 'add-korisnik',
  templateUrl: './add.component.html',
})

export class KorisniciAddComponent implements OnInit {

  public klijenti: Klijent[];
  public korisnik: Korisnici;
  public korisnikForm: FormGroup;
  public submitted: boolean = false;
  public selectedKlijent:number;

  get f() { return this.korisnikForm.controls; }

  constructor(private _flashMessagesService: FlashMessagesService, private formBuilder: FormBuilder, public _service: RestService) { }

  ngOnInit() {
    this.korisnikForm = this.formBuilder.group({
      imePrezime: ['', Validators.required],
      klijentId: [0, Validators.required],
      korisnickoIme: ['', Validators.required],
      lozinka: ['', Validators.required],
    });

    this._service.getKlijenti().subscribe((data: any) => {
      this.klijenti = data;
    });
  }

  submit() {
    if (this.korisnikForm.invalid) {
      this.submitted = true;
      return;
    }

    this.korisnik = new Korisnici();
    this.korisnik.imePrezime = this.f.imePrezime.value;
    this.korisnik.ime = this.f.korisnickoIme.value;
    this.korisnik.firma = this.f.klijentId.value;
    this.korisnik.lozinka = this.f.lozinka.value;
    this.korisnik.ulogaID = 1;
    this.korisnik.active = true;

    this._service.addKorisnik(this.korisnik).subscribe((data: any) => {
      if (data == true) {
        this._flashMessagesService.show(Resource.ADD_SUCCESS, { cssClass: 'alert-success', timeout: 5000 });
      }
      else {
        this._flashMessagesService.show(Resource.USERNAME_ERROR, { cssClass: 'alert-danger', timeout: 5000 });
      }
      this.ngOnInit();
    }, (err: HttpErrorResponse) => {
      this._flashMessagesService.show(Resource.ERROR, { cssClass: 'alert-danger', timeout: 5000 });
    });
  }
}
