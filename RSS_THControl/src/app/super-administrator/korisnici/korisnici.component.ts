import { Component, OnInit } from '@angular/core';
import { DataService } from '../../shared/services/data.service';
import { Router } from '../../../../node_modules/@angular/router';
import { RestService } from '../../shared/services/rest.service';
import { Resource } from '../../shared/resource';
import { FlashMessagesService } from '../../../../node_modules/angular2-flash-messages';

@Component({
  selector: 'korisnici',
  templateUrl: './korisnici.component.html',
})
export class KorisniciComponent implements OnInit {

  public p: number = 1;

  constructor(public _service: DataService, private router: Router, private _restService:RestService,
    private _flashMessagesService: FlashMessagesService) { }

  ngOnInit() {
    this._service.getKorisnike();
  }

  edit(korisnik) {
    this._service.selectedKorisnik = korisnik;
    this.router.navigate(['rss/korisnici/uredi', this._service.selectedKorisnik.id]);
  }

  changeStatus(korisnik){
    this._restService.changeKorisnikStatus(korisnik).subscribe((data:any)=>{
      this._flashMessagesService.show(Resource.STATUS_SUCCESS, { cssClass: 'alert-success', timeout: 5000 });
      this.ngOnInit();
    });
  }
}
