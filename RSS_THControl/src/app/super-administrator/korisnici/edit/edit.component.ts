import { Component, OnInit } from '@angular/core';
import { Klijent } from '../../../shared/models/klijent.model';
import { Korisnici } from '../../../shared/models/korisnici.model';
import { FormGroup, FormBuilder, Validators } from '../../../../../node_modules/@angular/forms';
import { FlashMessagesService } from '../../../../../node_modules/angular2-flash-messages';
import { RestService } from '../../../shared/services/rest.service';
import { DataService } from '../../../shared/services/data.service';
import { Resource } from '../../../shared/resource';
import { HttpErrorResponse } from '../../../../../node_modules/@angular/common/http';

@Component({
  selector: 'edit-korisnik',
  templateUrl: './edit.component.html',
})

export class KorisniciEditComponent implements OnInit {

  public klijenti: Klijent[];
  public korisnik: Korisnici;
  public korisnikForm: FormGroup;
  public submitted: boolean = false;
  public selectedKlijent:number;

  get f() { return this.korisnikForm.controls; }

  constructor(private _flashMessagesService: FlashMessagesService, private formBuilder: FormBuilder, public _service: RestService,
    public _dataService: DataService) { }

  ngOnInit() {
    this.korisnikForm = this.formBuilder.group({
      imePrezime: [this._dataService.selectedKorisnik.imePrezime, Validators.required],
      klijentId: [this._dataService.selectedKorisnik.firma, Validators.required],
      korisnickoIme: [this._dataService.selectedKorisnik.ime, Validators.required],
      lozinka: [''],
    });

    this._service.getKlijenti().subscribe((data: any) => {
      this.klijenti = data;
      this.korisnikForm.controls['klijentId'].setValue(this._dataService.selectedKorisnik.firma);
    });
  }

  submit() {
    if (this.korisnikForm.invalid) {
      this.submitted = true;
      return;
    }

    this.korisnik = this._dataService.selectedKorisnik;
    this.korisnik.imePrezime = this.f.imePrezime.value;
    this.korisnik.ime = this.f.korisnickoIme.value;
    this.korisnik.firma = this.f.klijentId.value;
    this.korisnik.lozinka = this.f.lozinka.value;
    this.korisnik.ulogaID = 1;

    console.log(this.korisnik);
    this._service.updateKorisnik(this.korisnik).subscribe((data: any) => {
      if (data == true) {
        this._flashMessagesService.show(Resource.UPDATE_SUCCESS, { cssClass: 'alert-success', timeout: 5000 });
      }
      else {
        this._flashMessagesService.show(Resource.USERNAME_ERROR, { cssClass: 'alert-danger', timeout: 5000 });
      }
    }, (err: HttpErrorResponse) => {
      this._flashMessagesService.show(Resource.ERROR, { cssClass: 'alert-danger', timeout: 5000 });
    });
  }
}
