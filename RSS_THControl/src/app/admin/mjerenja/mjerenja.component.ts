import { Component, OnInit } from '@angular/core';
import { DataService } from '../../shared/services/data.service';
import { Router } from '../../../../node_modules/@angular/router';
import { RestService } from '../../shared/services/rest.service';
import { Resource } from '../../shared/resource';
import { FlashMessagesService } from '../../../../node_modules/angular2-flash-messages';
import { NgxSpinnerService } from '../../../../node_modules/ngx-spinner';
import { FormGroup, FormBuilder, Validators } from '../../../../node_modules/@angular/forms';
import { Klijent } from '../../shared/models/klijent.model';
import { Uredjaji } from '../../shared/models/uredjaji.model';
import { Grupa } from '../../shared/models/grupa.model';

declare var $: any;

@Component({
    selector: 'mjerenja',
    templateUrl: './mjerenja.component.html',
})
export class MjerenjaComponent implements OnInit {

    public p: number = 1;
    public startDate: any;
    public endDate: any;
    public filterForm: FormGroup;
    public uredjaji: Uredjaji[];
    public selectedKlijent: number;
    public selectedGrupa: number;
    public selectedPodgrupa: number;
    public grupe: Grupa[];

    get f() { return this.filterForm.controls; }

    constructor(public _service: DataService, private router: Router, private _restService: RestService,
        private _flashMessagesService: FlashMessagesService, private spinner: NgxSpinnerService,
        private formBuilder: FormBuilder) { }

    ngOnInit() {
        this._service.mjerenja=[];
        var start = new Date();
        this.startDate = (start.getMonth() + 1) + '/' + start.getDate() + '/' + start.getFullYear();
        this.endDate = (start.getMonth() + 1) + '/' + start.getDate() + '/' + start.getFullYear();
        this.selectedKlijent = -1;
        this.selectedGrupa = -1;
        this.selectedPodgrupa = -1;

        $(document).ready(function () {
            $("#m_datepicker_2").datepicker({ todayHighlight: !0, orientation: "bottom left" });
            $("#m_datepicker_3").datepicker({ todayHighlight: !0, orientation: "bottom left" });
        });

        this.filterForm = this.formBuilder.group({
            startDate: [this.startDate, Validators.required],
            endDate: [this.endDate, Validators.required],
            uredjajId: [-1, Validators.required],
            grupaId: [-1, Validators.required],
            podgrupaId: [-1, Validators.required],
        });

        if (this._service.klijentId == undefined || this._service.klijentId == null)
            this._service.klijentId = 0;
        this._restService.getUredjaje(this._service.klijentId, 0,0).subscribe((data: any) => {
            this.uredjaji = data;
        });

        this._restService.getGrupe(this._service.klijentId).subscribe((data: any) => {
            this.grupe = data;
        });
    }

    onChange() {
        var grupaId = this.f.grupaId.value;
        this.selectedGrupa = grupaId;
        this._service.getPodgrupeByGroup(grupaId);
    }

    edit(korisnik) {
        this._service.selectedKorisnik = korisnik;
        this.router.navigate(['rss/korisnici/uredi', this._service.selectedKorisnik.id]);
    }

    changeStatus(korisnik) {
        this._restService.changeKorisnikStatus(korisnik).subscribe((data: any) => {
            this._flashMessagesService.show(Resource.STATUS_SUCCESS, { cssClass: 'alert-success', timeout: 5000 });
            this.ngOnInit();
        });
    }

    pretrazi(startDate, endDate) {
        var sDate = startDate.value;
        var eDate = endDate.value;
        var uredjajId = this.f.uredjajId.value;
        var grupaId = this.f.grupaId.value;
        var podgrupaId = this.f.podgrupaId.value;

        if (sDate == "" && eDate == "") {
            this.startDate = this.startDate;
            this.endDate = this.endDate;
        }

        if (sDate != "" || eDate != "") {
            if (sDate != "")
                this.startDate = sDate;
            if (eDate != "")
                this.endDate = eDate;
            if (sDate == "")
                this.startDate = this.endDate;
            else if (eDate == "")
                this.endDate = this.startDate;
        }

        if (uredjajId == -1)
            uredjajId = 0;

        if (grupaId == -1)
            grupaId = 0;

        if (podgrupaId == -1)
            podgrupaId = 0;

        this.spinner.show();
        this._restService.getMjerenja(this.startDate, this.endDate, uredjajId, grupaId, podgrupaId).subscribe((data: any) => {
            this._service.mjerenja = data;
            this.spinner.hide();
        });
    }
}
