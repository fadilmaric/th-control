import { Component, OnInit } from '@angular/core';
import { Klijent } from '../../../shared/models/klijent.model';
import { FormGroup, FormBuilder, Validators } from '../../../../../node_modules/@angular/forms';
import { HttpErrorResponse } from '../../../../../node_modules/@angular/common/http';
import { DataService } from '../../../shared/services/data.service';
import { RestService } from '../../../shared/services/rest.service';
import { FlashMessagesService } from '../../../../../node_modules/angular2-flash-messages';
import { Resource } from '../../../shared/resource';
import { Grupa } from '../../../shared/models/grupa.model';

@Component({
    selector: 'add-grupa',
    templateUrl: './add.component.html',
})

export class GrupaAddComponent implements OnInit {

    public grupa: Grupa;
    public grupaForm: FormGroup;
    public submitted: boolean = false;

    get f() { return this.grupaForm.controls; }

    constructor(private formBuilder: FormBuilder, public _service: RestService, private _flashMessagesService: FlashMessagesService,
        private _dataService: DataService) { }

    ngOnInit() {
        this.grupaForm = this.formBuilder.group({
            naziv: ['', Validators.required],
        });
    }

    submit() {
        if (this.grupaForm.invalid) {
            this.submitted = true;
            return;
        }

        this.grupa = new Grupa();
        this.grupa.naziv = this.f.naziv.value;
        this.grupa.klijentId =this._dataService.klijentId;

            this._service.addGrupa(this.grupa).subscribe((data: any) => {
                this._flashMessagesService.show(Resource.ADD_SUCCESS, { cssClass: 'alert-success', timeout: 5000 });
                this.ngOnInit();
            }, (err: HttpErrorResponse) => {
                this._flashMessagesService.show(Resource.ERROR, { cssClass: 'alert-danger', timeout: 5000 });
            });
    }
}
