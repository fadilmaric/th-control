import { Component, OnInit } from '@angular/core';
import { DataService } from '../../shared/services/data.service';
import { Router } from '../../../../node_modules/@angular/router';

@Component({
  selector: 'grupe',
  templateUrl: './grupe.component.html'
})
export class GrupeComponent implements OnInit{


public p=1;
  constructor(public _service: DataService, private router: Router){}

  ngOnInit(){
    this._service.getGrupe();
  }

  edit(grupa) {
    this._service.selectedGrupa = grupa;
    this.router.navigate(['admin/grupe/uredi', this._service.selectedGrupa.id]);
  }
}
