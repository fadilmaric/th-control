import { Component, OnInit } from '@angular/core';
import { Klijent } from '../../../shared/models/klijent.model';
import { FormGroup, FormBuilder, Validators } from '../../../../../node_modules/@angular/forms';
import { HttpErrorResponse } from '../../../../../node_modules/@angular/common/http';
import { DataService } from '../../../shared/services/data.service';
import { RestService } from '../../../shared/services/rest.service';
import { FlashMessagesService } from '../../../../../node_modules/angular2-flash-messages';
import { Resource } from '../../../shared/resource';
import { Grupa } from '../../../shared/models/grupa.model';

@Component({
    selector: 'edit-grupa',
    templateUrl: './edit.component.html',
})

export class GrupaEditComponent implements OnInit {

    public grupa: Grupa;
    public grupaForm: FormGroup;
    public submitted: boolean = false;

    get f() { return this.grupaForm.controls; }

    constructor(private formBuilder: FormBuilder, public _service: RestService, private _flashMessagesService: FlashMessagesService,
        public _dataService: DataService) { }

    ngOnInit() {
        this.grupaForm = this.formBuilder.group({
            naziv: [this._dataService.selectedGrupa.naziv, Validators.required],
        });
    }

    submit() {
        if (this.grupaForm.invalid) {
            this.submitted = true;
            return;
        }

        this.grupa = new Grupa();
        this.grupa.naziv = this.f.naziv.value;
        this.grupa.id = this._dataService.selectedGrupa.id;
        this.grupa.klijentId = this._dataService.selectedGrupa.klijentId;

        this._service.updateGrupa(this.grupa).subscribe((data: any) => {
            this._flashMessagesService.show(Resource.UPDATE_SUCCESS, { cssClass: 'alert-success', timeout: 5000 });
        }, (err: HttpErrorResponse) => {
            this._flashMessagesService.show(Resource.ERROR, { cssClass: 'alert-danger', timeout: 5000 });
        });
    }
}
