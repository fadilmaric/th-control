import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { DataService } from '../../shared/services/data.service';
import { RestService } from '../../shared/services/rest.service';
import { FormGroup, FormBuilder, Validators } from '../../../../node_modules/@angular/forms';
import { Grupa } from '../../shared/models/grupa.model';
import { isGeneratedFile } from '../../../../node_modules/@angular/compiler/src/aot/util';

@Component({
    selector: 'admin-dashboard',
    templateUrl: './dashboard.component.html',
})
export class AdminDashboardComponent implements OnInit {

    public p = 1;
    public p1 = 1;
    public o1 = 1;
    showXAxis1 = true;
    showYAxis1 = true;
    gradient1 = false;
    showLegend1 = true;
    showXAxisLabel1 = false;
    xAxisLabel1 = '';
    showYAxisLabel1 = true;
    yAxisLabel1 = '';
    timeline1 = false;
    legendTitle1 = "";
    view = [500, 300];
    public isChart = false;

    public filterForm: FormGroup;
    public selectedGrupa: number;
    public selectedPodgrupa: number;
    public grupe: Grupa[];

    get f() { return this.filterForm.controls; }

    multi1: any[] = [
        {
            name: 'Cyan',
            series: [
                {
                    name: 5,
                    value: 2650
                },
                {
                    name: 10,
                    value: 2800
                },
                {
                    name: 15,
                    value: 2000
                }
            ]
        },
        {
            name: 'Yellow',
            series: [
                {
                    name: 5,
                    value: 2500
                },
                {
                    name: 9,
                    value: 5100
                },
                {
                    name: 15,
                    value: 2350
                }
            ]
        }
    ];

    colorScheme1 = {
        domain: ['#6e339c', '#a099d5', '#75f65b', '#e9668b', '#ddf731', '#bcac18', '#3c4a74', '#2646f1', '#d62c73', '#f23d65', '#6e339c', '#6e339c', '#abaa9b', '#bcac18', '#579305']
    };
    constructor(public restService: RestService, private changeRef: ChangeDetectorRef, public _service: DataService,
        private formBuilder: FormBuilder, public _restService: RestService) { }

    openChart(uredjajId) {
        this.restService.getDetaljiLogera(uredjajId).subscribe((data: any) => {
            this.multi1 = data;
            this.isChart = true;
            this.changeRef.detectChanges();
        })
    }

    ngOnInit() {
        this._service.getTrenutnoStanje();
        this._service.checkMjerenja(0, 0);
        this._service.getAllCritical();

        this.filterForm = this.formBuilder.group({
            grupaId: [0, Validators.required],
            podgrupaId: [0, Validators.required],
        });

        this._restService.getGrupe(this._service.klijentId).subscribe((data: any) => {
            this.grupe = data;
        });
    }

    onChange() {
        var grupaId = this.f.grupaId.value;
        this.selectedGrupa = grupaId;
        this._service.getPodgrupeByGroup(grupaId);
    }

    pretrazi() {
        var grupaId = this.f.grupaId.value;
        var podgrupaId = this.f.podgrupaId.value;

        if (grupaId == undefined)
            grupaId = 0;

        if (podgrupaId == undefined)
            podgrupaId = 0;

        if (grupaId == 0)
            podgrupaId = 0;
        this._service.checkMjerenja(grupaId, podgrupaId);
    }
}
