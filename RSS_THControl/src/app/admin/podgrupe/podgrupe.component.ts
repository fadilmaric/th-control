import { Component, OnInit } from '@angular/core';
import { DataService } from '../../shared/services/data.service';
import { Router } from '../../../../node_modules/@angular/router';

@Component({
  selector: 'podgrupe',
  templateUrl: './podgrupe.component.html'
})
export class PodgrupeComponent implements OnInit{

public p=1;

  constructor(public _service: DataService, private router: Router){}

  ngOnInit(){
    this._service.getPodgrupe();
  }

  edit(podgrupa) {
    this._service.selectedPodgrupa = podgrupa;
    this.router.navigate(['admin/podgrupe/uredi', this._service.selectedPodgrupa.id]);
  }
}
