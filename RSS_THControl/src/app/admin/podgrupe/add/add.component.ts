import { Component, OnInit } from '@angular/core';
import { Klijent } from '../../../shared/models/klijent.model';
import { FormGroup, FormBuilder, Validators } from '../../../../../node_modules/@angular/forms';
import { HttpErrorResponse } from '../../../../../node_modules/@angular/common/http';
import { DataService } from '../../../shared/services/data.service';
import { RestService } from '../../../shared/services/rest.service';
import { FlashMessagesService } from '../../../../../node_modules/angular2-flash-messages';
import { Resource } from '../../../shared/resource';
import { Grupa } from '../../../shared/models/grupa.model';
import { Podgrupa } from '../../../shared/models/podgrupa.model';

@Component({
    selector: 'add-podgrupe',
    templateUrl: './add.component.html',
})

export class PodgrupeAddComponent implements OnInit {

    public grupa: Podgrupa;
    public grupaForm: FormGroup;
    public submitted: boolean = false;
    public grupe: Grupa[];
    public selectedKlijent:number;

    get f() { return this.grupaForm.controls; }

    constructor(private formBuilder: FormBuilder, public _service: RestService, private _flashMessagesService: FlashMessagesService,
        private _dataService: DataService) { }

    ngOnInit() {
        this.grupaForm = this.formBuilder.group({
            naziv: ['', Validators.required],
            grupaId: [0, Validators.required]
        });
        if (this._dataService.klijentId == undefined || this._dataService.klijentId == null)
            this._dataService.klijentId = 0;
        this._service.getGrupe(this._dataService.klijentId).subscribe((data: any) => {
            this.grupe = data;
        });
    }

    submit() {
        if (this.grupaForm.invalid) {
            this.submitted = true;
            return;
        }

        this.grupa = new Podgrupa();
        this.grupa.naziv = this.f.naziv.value;
        this.grupa.klijentId = this._dataService.klijentId;
        this.grupa.grupaId = this.f.grupaId.value;

        this._service.addPodgrupa(this.grupa).subscribe((data: any) => {
            this._flashMessagesService.show(Resource.ADD_SUCCESS, { cssClass: 'alert-success', timeout: 5000 });
            this.ngOnInit();
        }, (err: HttpErrorResponse) => {
            this._flashMessagesService.show(Resource.ERROR, { cssClass: 'alert-danger', timeout: 5000 });
        });
    }

}
