import { Component, OnInit } from '@angular/core';
import { DataService } from '../../shared/services/data.service';
import { Router } from '../../../../node_modules/@angular/router';
import { Grupa } from '../../shared/models/grupa.model';
import { FormGroup, FormBuilder, Validators } from '../../../../node_modules/@angular/forms';
import { RestService } from '../../shared/services/rest.service';

@Component({
    selector: 'uredjaji',
    templateUrl: './uredjaji.component.html',
})
export class UredjajiComponent implements OnInit {

    public p = 1;
    public filterForm: FormGroup;
    public selectedGrupa: number;
    public selectedPodgrupa: number;
    public grupe: Grupa[];

    get f() { return this.filterForm.controls; }

    constructor(public _service: DataService, private router: Router, private formBuilder: FormBuilder,
        public _restService: RestService) { }

    ngOnInit() {
        this._service.getUredjaje(0, 0);

        this.filterForm = this.formBuilder.group({
            grupaId: [0, Validators.required],
            podgrupaId: [0, Validators.required],
        });

        this._restService.getGrupe(this._service.klijentId).subscribe((data: any) => {
            this.grupe = data;
        });
    }


    onChange() {
        var grupaId = this.f.grupaId.value;
        this.selectedGrupa = grupaId;
        this._service.getPodgrupeByGroup(grupaId);
    }

    edit(uredjaj) {
        this._service.selectedUredjaj = uredjaj;
        if (this._service.selectedUredjaj.korisnici == null)
            this._service.selectedUredjaj.korisnici = [];

        this.router.navigate(['admin/uredjaji/uredi', this._service.selectedUredjaj.id]);
    }

    pretrazi() {
        var grupaId = this.f.grupaId.value;
        var podgrupaId = this.f.podgrupaId.value;

        if (grupaId == undefined)
            grupaId = 0;

        if (podgrupaId == undefined)
            podgrupaId = 0;

        if (grupaId == 0)
            podgrupaId = 0;

        this._service.getUredjaje(grupaId, podgrupaId);
    }
}
