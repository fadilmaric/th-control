import { Component, OnInit } from '@angular/core';
import { DataService } from '../../../shared/services/data.service';
import { FormGroup, FormBuilder, Validators, FormArray, FormControl } from '../../../../../node_modules/@angular/forms';
import { Korisnici } from '../../../shared/models/korisnici.model';
import { KorisniciUredjaji } from '../../../shared/models/korisniciuredjaji.model';
import { RestService } from '../../../shared/services/rest.service';
import { FlashMessagesService } from '../../../../../node_modules/angular2-flash-messages';
import { Resource } from '../../../shared/resource';
import { HttpErrorResponse } from '../../../../../node_modules/@angular/common/http';

declare var $: any;

@Component({
    selector: 'edit-uredjaj-korisnici',
    templateUrl: './edit.component.html',
})
export class UredjajiKorisniciEditComponent implements OnInit {

    public uredjajForm: FormGroup;
    public submitted: boolean = false;
    public selectedGrupa: number;
    public selectedKlijent:number;
    public korisnici: KorisniciUredjaji[];

    get f() { return this.uredjajForm.controls; }

    constructor(public _service: DataService, private formBuilder: FormBuilder, private _restService: RestService,
        private _flashMessagesService: FlashMessagesService) { }

    ngOnInit() {
        this._service.getGrupe();
        this._service.getPodgrupeByGroup(this._service.selectedUredjaj.grupaid);
        this.selectedGrupa=this._service.selectedUredjaj.grupaid;
        this.selectedKlijent=this._service.selectedUredjaj.podgrupaid;

        this.uredjajForm = this.formBuilder.group({
            grupaId: [this._service.selectedUredjaj.grupaid, Validators.required],
            podgrupaId: [this._service.selectedUredjaj.podgrupaid, Validators.required],
            tmin:[this._service.selectedUredjaj.tmin, Validators.required],
            tmax:[this._service.selectedUredjaj.tmax, Validators.required],
            hmin:[this._service.selectedUredjaj.hmin, Validators.required],
            hmax:[this._service.selectedUredjaj.hmax, Validators.required],
        });

        if (this._service.klijentId == undefined || this._service.klijentId == null)
            this._service.klijentId = 0;

        this.korisnici = this._service.selectedUredjaj.korisnici;
    }

    onChange() {
        var grupaId = this.f.grupaId.value;
        this.selectedGrupa = grupaId;
        this._service.getPodgrupeByGroup(grupaId);
    }

    checkZaposlenik(event) {
        if (event.target.checked) {
            for (var i = 0; i < this.korisnici.length; i++) {
                if (this.korisnici[i].id == parseInt(event.target.value)) {
                   this.korisnici[i].checked=true;
                }
            }
        }
        else {
            for (var i = 0; i < this.korisnici.length; i++) {
                if (this.korisnici[i].id == parseInt(event.target.value)) {
                   this.korisnici[i].checked=false;
                }
            }
        }
    }

    submit() {
        if (this.uredjajForm.invalid) {
            this.submitted = true;
            return;
        }

        this._service.selectedUredjaj.grupaid = this.f.grupaId.value;
        this._service.selectedUredjaj.podgrupaid = this.f.podgrupaId.value;

        this._service.selectedUredjaj.tmin=this.f.tmin.value;
        this._service.selectedUredjaj.tmax=this.f.tmax.value;
        this._service.selectedUredjaj.hmin=this.f.hmin.value;
        this._service.selectedUredjaj.hmax=this.f.hmax.value;

        this._service.selectedUredjaj.korisnici=this.korisnici;

        this._restService.updateKorisnikeLogere(this._service.selectedUredjaj).subscribe((data: any) => {
            this._flashMessagesService.show(Resource.UPDATE_SUCCESS, { cssClass: 'alert-success', timeout: 5000 });
        }, (err: HttpErrorResponse) => {
            this._flashMessagesService.show(Resource.ERROR, { cssClass: 'alert-danger', timeout: 5000 });
        });
    }
}
