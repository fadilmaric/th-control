﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using RSS_THControl_DAL.Contexts;
using RSS_THControl_DAL.Repositories;
using RSS_THControl_DAL.Repositories.IRepositories;
using RSS_THControl_DB.Models;

namespace RSS_THControl.Controllers
{
    [Route("api/korisnici")]
    [ApiController]
    public class KorisniciController : ControllerBase
    {
        public readonly IKorisniciRepository korisniciRepository;

        public KorisniciController(Context context)
        {
            this.korisniciRepository = new KorisniciRepository(context);
        }
        [HttpGet]
        [Route("Login")]
        public IActionResult Login(string username,string password)
        {
            return Ok(korisniciRepository.Login(username,password));
        }
        [HttpGet]
        [Route("GetAll")]
        public IActionResult GetAll(int ulogaID, int klijentID)
        {
            return Ok(korisniciRepository.GetAll(ulogaID,klijentID));
        }
        [HttpPost]
        [Route("Insert")]
        public IActionResult Insert([FromBody] Korisnici k)
        {
            return Ok(korisniciRepository.Insert(k));
        }
        [HttpPost]
        [Route("Edit")]
        public IActionResult Edit([FromBody] Korisnici k)
        {
            return Ok(korisniciRepository.Edit(k));
        }
        [HttpPost]
        [Route("ChangeStatus")]
        public IActionResult ChangeStatus([FromBody] Korisnici k)
        {
            return Ok(korisniciRepository.ChangeStatus(k));
        }
    }
}