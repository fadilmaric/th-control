﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using RSS_THControl_DAL.Contexts;
using RSS_THControl_DAL.Repositories;
using RSS_THControl_DAL.Repositories.IRepositories;

namespace RSS_THControl.Controllers
{
    [Route("api/mjerenja")]
    [ApiController]
    public class MjerenjaController : ControllerBase
    {
        public readonly IMjerenjaRepository mjerenjaRepository;

        public MjerenjaController(Context context)
        {
            this.mjerenjaRepository = new MjerenjaRepository(context);
        }
        [HttpGet]
        [Route("GetAll")]
        public IActionResult GetAll(DateTime datumOd, DateTime datumDo, int logerID,int grupaID,int podgrupaID)
        {
            return Ok(mjerenjaRepository.GetAll(datumOd,datumDo,logerID,grupaID,podgrupaID));
        }
        [HttpGet]
        [Route("GetAllCritical")]
        public IActionResult GetAllCritical()
        {
            return Ok(mjerenjaRepository.GetAllCritical());
        }
        [HttpGet]
        [Route("GetDetaljeZaLoger")]
        public IActionResult GetDetaljeZaLoger(int logerID)
        {
            return Ok(mjerenjaRepository.GetDetaljeZaLoger(logerID));
        }
    }
}