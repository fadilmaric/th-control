﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using RSS_THControl_DAL.Contexts;
using RSS_THControl_DAL.Contexts.DTO;
using RSS_THControl_DAL.Repositories;
using RSS_THControl_DAL.Repositories.IRepositories;
using RSS_THControl_DB.Models;

namespace RSS_THControl.Controllers
{
    [Route("api/logeri")]
    [ApiController]
    public class LogeriController : ControllerBase
    {
        public readonly ILogeriRepository logeriRepository;

        public LogeriController(Context context)
        {
            this.logeriRepository = new LogeriRepository(context);
        }
        [HttpGet]
        [Route("GetAll")]
        public IActionResult GetAll(int klijentID, int grupaID, int podgrupaID)
        {
            return Ok(logeriRepository.GetAll(klijentID,grupaID,podgrupaID));
        }
        [HttpGet]
        [Route("GetAllByUser")]
        public IActionResult GetAllByUser(int korisnikID)
        {
            return Ok(logeriRepository.GetAllByUser(korisnikID));
        }
        [HttpPost]
        [Route("Insert")]
        public IActionResult Insert([FromBody] Logeri l)
        {
            return Ok(logeriRepository.Insert(l));
        }
        [HttpPost]
        [Route("Edit")]
        public IActionResult Edit([FromBody] Logeri k)
        {
            return Ok(logeriRepository.Edit(k));
        }
        [HttpPost]
        [Route("EditLogeriKorisnici")]
        public IActionResult EditLogeriKorisnici([FromBody] LogeriVM k)
        {
            return Ok(logeriRepository.EditLogeriKorisnici(k));
        }
        [HttpGet]
        [Route("ArduinoData")]
        public IActionResult ArduinoData(int IDUredjaja, string temp, string Hum)
        {
            return Ok(logeriRepository.ArduinoData(IDUredjaja, temp, Hum));
        }
        [HttpGet]
        [Route("ProvjeraMjerenja")]
        public IActionResult ProvjeraMjerenja(int klijentID, int grupaID, int podgrupaID)
        {
            return Ok(logeriRepository.ProvjeraMjerenja(klijentID,grupaID,podgrupaID));
        }
        [HttpGet]
        [Route("TrenutnoStanje")]
        public IActionResult TrenutnoStanje(int klijentID)
        {
            return Ok(logeriRepository.TrenutnoStanje(klijentID));
        }
        [HttpGet]
        [Route("TrenutnoStanjeZaKorisnika")]
        public IActionResult TrenutnoStanjeZaKorisnika(int korisnikID)
        {
            return Ok(logeriRepository.TrenutnoStanjeZaKorisnika(korisnikID));
        }
    }
}