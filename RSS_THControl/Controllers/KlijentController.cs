﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using RSS_THControl_DAL.Contexts;
using RSS_THControl_DAL.Repositories;
using RSS_THControl_DAL.Repositories.IRepositories;
using RSS_THControl_DB.Models;

namespace RSS_THControl.Controllers
{
    [Route("api/klijenti")]
    [ApiController]
    public class KlijentController : ControllerBase
    {
        public readonly IKlijentRepository klijentRepository;

        public KlijentController(Context context)
        {
            this.klijentRepository = new KlijentRepository(context);
        }


        [HttpGet]
        [Route("GetAll")]
        public IActionResult GetAll()
        {
           return Ok(klijentRepository.GetAll());
        }
        [HttpPost]
        [Route("Insert")]
        public IActionResult Insert([FromBody] Klijenti k)
        {
            return Ok(klijentRepository.Insert(k));
        }

        [HttpPost]
        [Route("Edit")]
        public IActionResult Edit([FromBody] Klijenti k)
        {
            return Ok(klijentRepository.Edit(k));
        }
        [HttpPost]
        [Route("ChangeStatus")]
        public IActionResult ChangeStatus([FromBody] Klijenti k)
        {
            return Ok(klijentRepository.ChangeStatus(k));
        }

        [HttpGet]
        [Route("CountLogeraPoKlijentu")]
        public IActionResult CountLogeraPoKlijentu()
        {
            return Ok(klijentRepository.CountLogeraPoKlijentu());
        }
    }
}