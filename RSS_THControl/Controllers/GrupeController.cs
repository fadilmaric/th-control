﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using RSS_THControl_DAL.Contexts;
using RSS_THControl_DAL.Repositories;
using RSS_THControl_DAL.Repositories.IRepositories;
using RSS_THControl_DB.Models;

namespace RSS_THControl.Controllers
{
    [Route("api/grupe")]
    [ApiController]
    public class GrupeController : ControllerBase
    {
        public readonly IGrupeRepository grupeRepository;

        public GrupeController(Context context)
        {
            this.grupeRepository = new GrupeRepository(context);
        }
        [HttpGet]
        [Route("GetAll")]
        public IActionResult GetAll(int klijentID)
        {
            return Ok(grupeRepository.GetAll(klijentID));
        }
        [HttpPost]
        [Route("Insert")]
        public IActionResult Insert([FromBody] Grupa k)
        {
            return Ok(grupeRepository.Insert(k));
        }
        [HttpPost]
        [Route("Edit")]
        public IActionResult Edit([FromBody] Grupa k)
        {
            return Ok(grupeRepository.Edit(k));
        }
    }
}