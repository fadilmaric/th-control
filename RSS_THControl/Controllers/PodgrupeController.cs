﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using RSS_THControl_DAL.Contexts;
using RSS_THControl_DAL.Repositories;
using RSS_THControl_DAL.Repositories.IRepositories;
using RSS_THControl_DB.Models;

namespace RSS_THControl.Controllers
{
    [Route("api/podgrupe")]
    [ApiController]
    public class PodgrupeController : ControllerBase
    {
        public readonly IPodgrupeRepository podgrupeRepository;

        public PodgrupeController(Context context)
        {
            this.podgrupeRepository = new PodgrupeRepository(context);
        }
        [HttpGet]
        [Route("GetAll")]
        public IActionResult GetAll(int klijentID)
        {
            return Ok(podgrupeRepository.GetAll(klijentID));
        }
        [HttpPost]
        [Route("Insert")]
        public IActionResult Insert([FromBody] PodGrupa k)
        {
            return Ok(podgrupeRepository.Insert(k));
        }
        [HttpPost]
        [Route("Edit")]
        public IActionResult Edit([FromBody] PodGrupa k)
        {
            return Ok(podgrupeRepository.Edit(k));
        }
    }
}