var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { DataService } from '../../../shared/services/data.service';
import { FormBuilder, Validators } from '../../../../../node_modules/@angular/forms';
import { RestService } from '../../../shared/services/rest.service';
import { FlashMessagesService } from '../../../../../node_modules/angular2-flash-messages';
import { Resource } from '../../../shared/resource';
var UredjajiKorisniciEditComponent = /** @class */ (function () {
    function UredjajiKorisniciEditComponent(_service, formBuilder, _restService, _flashMessagesService) {
        this._service = _service;
        this.formBuilder = formBuilder;
        this._restService = _restService;
        this._flashMessagesService = _flashMessagesService;
        this.submitted = false;
    }
    Object.defineProperty(UredjajiKorisniciEditComponent.prototype, "f", {
        get: function () { return this.uredjajForm.controls; },
        enumerable: true,
        configurable: true
    });
    UredjajiKorisniciEditComponent.prototype.ngOnInit = function () {
        this._service.getGrupe();
        this._service.getPodgrupeByGroup(this._service.selectedUredjaj.grupaid);
        this.selectedGrupa = this._service.selectedUredjaj.grupaid;
        this.selectedKlijent = this._service.selectedUredjaj.podgrupaid;
        this.uredjajForm = this.formBuilder.group({
            grupaId: [this._service.selectedUredjaj.grupaid, Validators.required],
            podgrupaId: [this._service.selectedUredjaj.podgrupaid, Validators.required],
            tmin: [this._service.selectedUredjaj.tmin, Validators.required],
            tmax: [this._service.selectedUredjaj.tmax, Validators.required],
            hmin: [this._service.selectedUredjaj.hmin, Validators.required],
            hmax: [this._service.selectedUredjaj.hmax, Validators.required],
        });
        if (this._service.klijentId == undefined || this._service.klijentId == null)
            this._service.klijentId = 0;
        this.korisnici = this._service.selectedUredjaj.korisnici;
    };
    UredjajiKorisniciEditComponent.prototype.onChange = function () {
        var grupaId = this.f.grupaId.value;
        this.selectedGrupa = grupaId;
        this._service.getPodgrupeByGroup(grupaId);
    };
    UredjajiKorisniciEditComponent.prototype.checkZaposlenik = function (event) {
        if (event.target.checked) {
            for (var i = 0; i < this.korisnici.length; i++) {
                if (this.korisnici[i].id == parseInt(event.target.value)) {
                    this.korisnici[i].checked = true;
                }
            }
        }
        else {
            for (var i = 0; i < this.korisnici.length; i++) {
                if (this.korisnici[i].id == parseInt(event.target.value)) {
                    this.korisnici[i].checked = false;
                }
            }
        }
    };
    UredjajiKorisniciEditComponent.prototype.submit = function () {
        var _this = this;
        if (this.uredjajForm.invalid) {
            this.submitted = true;
            return;
        }
        this._service.selectedUredjaj.grupaid = this.f.grupaId.value;
        this._service.selectedUredjaj.podgrupaid = this.f.podgrupaId.value;
        this._service.selectedUredjaj.tmin = this.f.tmin.value;
        this._service.selectedUredjaj.tmax = this.f.tmax.value;
        this._service.selectedUredjaj.hmin = this.f.hmin.value;
        this._service.selectedUredjaj.hmax = this.f.hmax.value;
        this._service.selectedUredjaj.korisnici = this.korisnici;
        this._restService.updateKorisnikeLogere(this._service.selectedUredjaj).subscribe(function (data) {
            _this._flashMessagesService.show(Resource.UPDATE_SUCCESS, { cssClass: 'alert-success', timeout: 5000 });
        }, function (err) {
            _this._flashMessagesService.show(Resource.ERROR, { cssClass: 'alert-danger', timeout: 5000 });
        });
    };
    UredjajiKorisniciEditComponent = __decorate([
        Component({
            selector: 'edit-uredjaj-korisnici',
            templateUrl: './edit.component.html',
        }),
        __metadata("design:paramtypes", [DataService, FormBuilder, RestService,
            FlashMessagesService])
    ], UredjajiKorisniciEditComponent);
    return UredjajiKorisniciEditComponent;
}());
export { UredjajiKorisniciEditComponent };
//# sourceMappingURL=edit.component.js.map