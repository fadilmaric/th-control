var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { DataService } from '../../shared/services/data.service';
import { Router } from '../../../../node_modules/@angular/router';
import { FormBuilder, Validators } from '../../../../node_modules/@angular/forms';
import { RestService } from '../../shared/services/rest.service';
var UredjajiComponent = /** @class */ (function () {
    function UredjajiComponent(_service, router, formBuilder, _restService) {
        this._service = _service;
        this.router = router;
        this.formBuilder = formBuilder;
        this._restService = _restService;
        this.p = 1;
    }
    Object.defineProperty(UredjajiComponent.prototype, "f", {
        get: function () { return this.filterForm.controls; },
        enumerable: true,
        configurable: true
    });
    UredjajiComponent.prototype.ngOnInit = function () {
        var _this = this;
        this._service.getUredjaje(0, 0);
        this.filterForm = this.formBuilder.group({
            grupaId: [0, Validators.required],
            podgrupaId: [0, Validators.required],
        });
        this._restService.getGrupe(this._service.klijentId).subscribe(function (data) {
            _this.grupe = data;
        });
    };
    UredjajiComponent.prototype.onChange = function () {
        var grupaId = this.f.grupaId.value;
        this.selectedGrupa = grupaId;
        this._service.getPodgrupeByGroup(grupaId);
    };
    UredjajiComponent.prototype.edit = function (uredjaj) {
        this._service.selectedUredjaj = uredjaj;
        if (this._service.selectedUredjaj.korisnici == null)
            this._service.selectedUredjaj.korisnici = [];
        this.router.navigate(['admin/uredjaji/uredi', this._service.selectedUredjaj.id]);
    };
    UredjajiComponent.prototype.pretrazi = function () {
        var grupaId = this.f.grupaId.value;
        var podgrupaId = this.f.podgrupaId.value;
        if (grupaId == undefined)
            grupaId = 0;
        if (podgrupaId == undefined)
            podgrupaId = 0;
        if (grupaId == 0)
            podgrupaId = 0;
        this._service.getUredjaje(grupaId, podgrupaId);
    };
    UredjajiComponent = __decorate([
        Component({
            selector: 'uredjaji',
            templateUrl: './uredjaji.component.html',
        }),
        __metadata("design:paramtypes", [DataService, Router, FormBuilder,
            RestService])
    ], UredjajiComponent);
    return UredjajiComponent;
}());
export { UredjajiComponent };
//# sourceMappingURL=uredjaji.component.js.map