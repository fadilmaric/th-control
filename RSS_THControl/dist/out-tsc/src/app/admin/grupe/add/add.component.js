var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { FormBuilder, Validators } from '../../../../../node_modules/@angular/forms';
import { DataService } from '../../../shared/services/data.service';
import { RestService } from '../../../shared/services/rest.service';
import { FlashMessagesService } from '../../../../../node_modules/angular2-flash-messages';
import { Resource } from '../../../shared/resource';
import { Grupa } from '../../../shared/models/grupa.model';
var GrupaAddComponent = /** @class */ (function () {
    function GrupaAddComponent(formBuilder, _service, _flashMessagesService, _dataService) {
        this.formBuilder = formBuilder;
        this._service = _service;
        this._flashMessagesService = _flashMessagesService;
        this._dataService = _dataService;
        this.submitted = false;
    }
    Object.defineProperty(GrupaAddComponent.prototype, "f", {
        get: function () { return this.grupaForm.controls; },
        enumerable: true,
        configurable: true
    });
    GrupaAddComponent.prototype.ngOnInit = function () {
        this.grupaForm = this.formBuilder.group({
            naziv: ['', Validators.required],
        });
    };
    GrupaAddComponent.prototype.submit = function () {
        var _this = this;
        if (this.grupaForm.invalid) {
            this.submitted = true;
            return;
        }
        this.grupa = new Grupa();
        this.grupa.naziv = this.f.naziv.value;
        this.grupa.klijentId = this._dataService.klijentId;
        this._service.addGrupa(this.grupa).subscribe(function (data) {
            _this._flashMessagesService.show(Resource.ADD_SUCCESS, { cssClass: 'alert-success', timeout: 5000 });
            _this.ngOnInit();
        }, function (err) {
            _this._flashMessagesService.show(Resource.ERROR, { cssClass: 'alert-danger', timeout: 5000 });
        });
    };
    GrupaAddComponent = __decorate([
        Component({
            selector: 'add-grupa',
            templateUrl: './add.component.html',
        }),
        __metadata("design:paramtypes", [FormBuilder, RestService, FlashMessagesService,
            DataService])
    ], GrupaAddComponent);
    return GrupaAddComponent;
}());
export { GrupaAddComponent };
//# sourceMappingURL=add.component.js.map