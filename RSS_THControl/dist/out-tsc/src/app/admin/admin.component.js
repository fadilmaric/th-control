var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { DataService } from '../shared/services/data.service';
var AdminComponent = /** @class */ (function () {
    function AdminComponent(router, _service) {
        this.router = router;
        this._service = _service;
        this.title = 'Vodovod';
        this.isOpenSidebar = false;
        this.isOpenToggle = false;
    }
    AdminComponent.prototype.ngOnInit = function () {
    };
    AdminComponent.prototype.logout = function () {
        this._service.logout();
    };
    AdminComponent.prototype.openSidebar = function () {
        var self = this;
        if (!self.isOpenSidebar) {
            document.getElementById("m_aside_left").style.left = "0";
            document.getElementById("m_aside_left").style.visibility = "visible";
            $(document).ready(function () {
                $(".m-aside-left-close").css("left", "230px");
            });
            self.isOpenSidebar = true;
        }
        else {
            document.getElementById("m_aside_left").style.left = "-265px";
            document.getElementById("m_aside_left").style.visibility = "hidden";
            $(document).ready(function () {
                $(".m-aside-left-close").css("left", "-25px");
            });
            self.isOpenSidebar = false;
        }
    };
    AdminComponent.prototype.openHeader = function () {
        var self = this;
        if (!self.isOpenToggle) {
            $("body").addClass("m-topbar--on");
            self.isOpenToggle = true;
        }
        else {
            $("body").removeClass("m-topbar--on");
            self.isOpenToggle = false;
        }
    };
    AdminComponent.prototype.closeSidebar = function () {
        var self = this;
        document.getElementById("m_aside_left").style.left = "-265px";
        document.getElementById("m_aside_left").style.visibility = "hidden";
        $(document).ready(function () {
            $(".m-aside-left-close").css("left", "-25px");
        });
        self.isOpenSidebar = false;
    };
    AdminComponent.prototype.onResize = function (event) {
        var self = this;
        if (event.target.innerWidth > 1024) {
            document.getElementById("m_aside_left").style.left = "0";
            document.getElementById("m_aside_left").style.visibility = "visible";
            $(document).ready(function () {
                $(".m-aside-left-close").css("left", "230px");
            });
            self.isOpenSidebar = true;
        }
        else {
            document.getElementById("m_aside_left").style.left = "-265px";
            document.getElementById("m_aside_left").style.visibility = "hidden";
            $(document).ready(function () {
                $(".m-aside-left-close").css("left", "-25px");
            });
            self.isOpenSidebar = false;
        }
    };
    AdminComponent = __decorate([
        Component({
            selector: 'admin',
            templateUrl: './admin.component.html'
        }),
        __metadata("design:paramtypes", [Router, DataService])
    ], AdminComponent);
    return AdminComponent;
}());
export { AdminComponent };
//# sourceMappingURL=admin.component.js.map