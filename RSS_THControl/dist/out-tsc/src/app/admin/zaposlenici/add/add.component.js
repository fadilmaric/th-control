var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { Korisnici } from '../../../shared/models/korisnici.model';
import { FormBuilder, Validators } from '../../../../../node_modules/@angular/forms';
import { RestService } from '../../../shared/services/rest.service';
import { FlashMessagesService } from '../../../../../node_modules/angular2-flash-messages';
import { Resource } from '../../../shared/resource';
import { DataService } from '../../../shared/services/data.service';
var ZaposleniciAddComponent = /** @class */ (function () {
    function ZaposleniciAddComponent(_flashMessagesService, formBuilder, _service, _dataService) {
        this._flashMessagesService = _flashMessagesService;
        this.formBuilder = formBuilder;
        this._service = _service;
        this._dataService = _dataService;
        this.submitted = false;
    }
    Object.defineProperty(ZaposleniciAddComponent.prototype, "f", {
        get: function () { return this.korisnikForm.controls; },
        enumerable: true,
        configurable: true
    });
    ZaposleniciAddComponent.prototype.ngOnInit = function () {
        this.korisnikForm = this.formBuilder.group({
            imePrezime: ['', Validators.required],
            korisnickoIme: ['', Validators.required],
            lozinka: ['', Validators.required],
        });
    };
    ZaposleniciAddComponent.prototype.submit = function () {
        var _this = this;
        if (this.korisnikForm.invalid) {
            this.submitted = true;
            return;
        }
        this.korisnik = new Korisnici();
        this.korisnik.imePrezime = this.f.imePrezime.value;
        this.korisnik.ime = this.f.korisnickoIme.value;
        this.korisnik.firma = this._dataService.klijentId;
        this.korisnik.lozinka = this.f.lozinka.value;
        this.korisnik.ulogaID = 2;
        this.korisnik.active = true;
        this._service.addKorisnik(this.korisnik).subscribe(function (data) {
            if (data == true) {
                _this._flashMessagesService.show(Resource.ADD_SUCCESS, { cssClass: 'alert-success', timeout: 5000 });
            }
            else {
                _this._flashMessagesService.show(Resource.USERNAME_ERROR, { cssClass: 'alert-danger', timeout: 5000 });
            }
            _this.ngOnInit();
        }, function (err) {
            _this._flashMessagesService.show(Resource.ERROR, { cssClass: 'alert-danger', timeout: 5000 });
        });
    };
    ZaposleniciAddComponent = __decorate([
        Component({
            selector: 'add-zaposlenik',
            templateUrl: './add.component.html',
        }),
        __metadata("design:paramtypes", [FlashMessagesService, FormBuilder, RestService,
            DataService])
    ], ZaposleniciAddComponent);
    return ZaposleniciAddComponent;
}());
export { ZaposleniciAddComponent };
//# sourceMappingURL=add.component.js.map