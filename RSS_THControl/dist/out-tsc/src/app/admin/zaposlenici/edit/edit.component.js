var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { Korisnici } from '../../../shared/models/korisnici.model';
import { FormBuilder, Validators } from '../../../../../node_modules/@angular/forms';
import { RestService } from '../../../shared/services/rest.service';
import { FlashMessagesService } from '../../../../../node_modules/angular2-flash-messages';
import { Resource } from '../../../shared/resource';
import { DataService } from '../../../shared/services/data.service';
var ZaposleniciEditComponent = /** @class */ (function () {
    function ZaposleniciEditComponent(_flashMessagesService, formBuilder, _service, _dataService) {
        this._flashMessagesService = _flashMessagesService;
        this.formBuilder = formBuilder;
        this._service = _service;
        this._dataService = _dataService;
        this.submitted = false;
    }
    Object.defineProperty(ZaposleniciEditComponent.prototype, "f", {
        get: function () { return this.korisnikForm.controls; },
        enumerable: true,
        configurable: true
    });
    ZaposleniciEditComponent.prototype.ngOnInit = function () {
        this.korisnikForm = this.formBuilder.group({
            imePrezime: [this._dataService.selectedKorisnik.imePrezime, Validators.required],
            korisnickoIme: [this._dataService.selectedKorisnik.ime, Validators.required],
            lozinka: [''],
        });
    };
    ZaposleniciEditComponent.prototype.submit = function () {
        var _this = this;
        if (this.korisnikForm.invalid) {
            this.submitted = true;
            return;
        }
        this.korisnik = new Korisnici();
        this.korisnik.id = this._dataService.selectedKorisnik.id;
        this.korisnik.imePrezime = this.f.imePrezime.value;
        this.korisnik.ime = this.f.korisnickoIme.value;
        this.korisnik.firma = this._dataService.klijentId;
        this.korisnik.lozinka = this.f.lozinka.value;
        this.korisnik.ulogaID = 2;
        this._service.updateKorisnik(this.korisnik).subscribe(function (data) {
            if (data == true) {
                _this._flashMessagesService.show(Resource.UPDATE_SUCCESS, { cssClass: 'alert-success', timeout: 5000 });
            }
            else {
                _this._flashMessagesService.show(Resource.USERNAME_ERROR, { cssClass: 'alert-danger', timeout: 5000 });
            }
        }, function (err) {
            _this._flashMessagesService.show(Resource.ERROR, { cssClass: 'alert-danger', timeout: 5000 });
        });
    };
    ZaposleniciEditComponent = __decorate([
        Component({
            selector: 'edit-zaposlenik',
            templateUrl: './edit.component.html',
        }),
        __metadata("design:paramtypes", [FlashMessagesService, FormBuilder, RestService,
            DataService])
    ], ZaposleniciEditComponent);
    return ZaposleniciEditComponent;
}());
export { ZaposleniciEditComponent };
//# sourceMappingURL=edit.component.js.map