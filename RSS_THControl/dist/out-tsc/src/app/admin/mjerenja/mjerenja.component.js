var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { DataService } from '../../shared/services/data.service';
import { Router } from '../../../../node_modules/@angular/router';
import { RestService } from '../../shared/services/rest.service';
import { Resource } from '../../shared/resource';
import { FlashMessagesService } from '../../../../node_modules/angular2-flash-messages';
import { NgxSpinnerService } from '../../../../node_modules/ngx-spinner';
import { FormBuilder, Validators } from '../../../../node_modules/@angular/forms';
var MjerenjaComponent = /** @class */ (function () {
    function MjerenjaComponent(_service, router, _restService, _flashMessagesService, spinner, formBuilder) {
        this._service = _service;
        this.router = router;
        this._restService = _restService;
        this._flashMessagesService = _flashMessagesService;
        this.spinner = spinner;
        this.formBuilder = formBuilder;
        this.p = 1;
    }
    Object.defineProperty(MjerenjaComponent.prototype, "f", {
        get: function () { return this.filterForm.controls; },
        enumerable: true,
        configurable: true
    });
    MjerenjaComponent.prototype.ngOnInit = function () {
        var _this = this;
        this._service.mjerenja = [];
        var start = new Date();
        this.startDate = (start.getMonth() + 1) + '/' + start.getDate() + '/' + start.getFullYear();
        this.endDate = (start.getMonth() + 1) + '/' + start.getDate() + '/' + start.getFullYear();
        this.selectedKlijent = -1;
        this.selectedGrupa = -1;
        this.selectedPodgrupa = -1;
        $(document).ready(function () {
            $("#m_datepicker_2").datepicker({ todayHighlight: !0, orientation: "bottom left" });
            $("#m_datepicker_3").datepicker({ todayHighlight: !0, orientation: "bottom left" });
        });
        this.filterForm = this.formBuilder.group({
            startDate: [this.startDate, Validators.required],
            endDate: [this.endDate, Validators.required],
            uredjajId: [-1, Validators.required],
            grupaId: [-1, Validators.required],
            podgrupaId: [-1, Validators.required],
        });
        if (this._service.klijentId == undefined || this._service.klijentId == null)
            this._service.klijentId = 0;
        this._restService.getUredjaje(this._service.klijentId, 0, 0).subscribe(function (data) {
            _this.uredjaji = data;
        });
        this._restService.getGrupe(this._service.klijentId).subscribe(function (data) {
            _this.grupe = data;
        });
    };
    MjerenjaComponent.prototype.onChange = function () {
        var grupaId = this.f.grupaId.value;
        this.selectedGrupa = grupaId;
        this._service.getPodgrupeByGroup(grupaId);
    };
    MjerenjaComponent.prototype.edit = function (korisnik) {
        this._service.selectedKorisnik = korisnik;
        this.router.navigate(['rss/korisnici/uredi', this._service.selectedKorisnik.id]);
    };
    MjerenjaComponent.prototype.changeStatus = function (korisnik) {
        var _this = this;
        this._restService.changeKorisnikStatus(korisnik).subscribe(function (data) {
            _this._flashMessagesService.show(Resource.STATUS_SUCCESS, { cssClass: 'alert-success', timeout: 5000 });
            _this.ngOnInit();
        });
    };
    MjerenjaComponent.prototype.pretrazi = function (startDate, endDate) {
        var _this = this;
        var sDate = startDate.value;
        var eDate = endDate.value;
        var uredjajId = this.f.uredjajId.value;
        var grupaId = this.f.grupaId.value;
        var podgrupaId = this.f.podgrupaId.value;
        if (sDate == "" && eDate == "") {
            this.startDate = this.startDate;
            this.endDate = this.endDate;
        }
        if (sDate != "" || eDate != "") {
            if (sDate != "")
                this.startDate = sDate;
            if (eDate != "")
                this.endDate = eDate;
            if (sDate == "")
                this.startDate = this.endDate;
            else if (eDate == "")
                this.endDate = this.startDate;
        }
        if (uredjajId == -1)
            uredjajId = 0;
        if (grupaId == -1)
            grupaId = 0;
        if (podgrupaId == -1)
            podgrupaId = 0;
        this.spinner.show();
        this._restService.getMjerenja(this.startDate, this.endDate, uredjajId, grupaId, podgrupaId).subscribe(function (data) {
            _this._service.mjerenja = data;
            _this.spinner.hide();
        });
    };
    MjerenjaComponent = __decorate([
        Component({
            selector: 'mjerenja',
            templateUrl: './mjerenja.component.html',
        }),
        __metadata("design:paramtypes", [DataService, Router, RestService,
            FlashMessagesService, NgxSpinnerService,
            FormBuilder])
    ], MjerenjaComponent);
    return MjerenjaComponent;
}());
export { MjerenjaComponent };
//# sourceMappingURL=mjerenja.component.js.map