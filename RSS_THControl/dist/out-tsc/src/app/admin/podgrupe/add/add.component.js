var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { FormBuilder, Validators } from '../../../../../node_modules/@angular/forms';
import { DataService } from '../../../shared/services/data.service';
import { RestService } from '../../../shared/services/rest.service';
import { FlashMessagesService } from '../../../../../node_modules/angular2-flash-messages';
import { Resource } from '../../../shared/resource';
import { Podgrupa } from '../../../shared/models/podgrupa.model';
var PodgrupeAddComponent = /** @class */ (function () {
    function PodgrupeAddComponent(formBuilder, _service, _flashMessagesService, _dataService) {
        this.formBuilder = formBuilder;
        this._service = _service;
        this._flashMessagesService = _flashMessagesService;
        this._dataService = _dataService;
        this.submitted = false;
    }
    Object.defineProperty(PodgrupeAddComponent.prototype, "f", {
        get: function () { return this.grupaForm.controls; },
        enumerable: true,
        configurable: true
    });
    PodgrupeAddComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.grupaForm = this.formBuilder.group({
            naziv: ['', Validators.required],
            grupaId: [0, Validators.required]
        });
        if (this._dataService.klijentId == undefined || this._dataService.klijentId == null)
            this._dataService.klijentId = 0;
        this._service.getGrupe(this._dataService.klijentId).subscribe(function (data) {
            _this.grupe = data;
        });
    };
    PodgrupeAddComponent.prototype.submit = function () {
        var _this = this;
        if (this.grupaForm.invalid) {
            this.submitted = true;
            return;
        }
        this.grupa = new Podgrupa();
        this.grupa.naziv = this.f.naziv.value;
        this.grupa.klijentId = this._dataService.klijentId;
        this.grupa.grupaId = this.f.grupaId.value;
        this._service.addPodgrupa(this.grupa).subscribe(function (data) {
            _this._flashMessagesService.show(Resource.ADD_SUCCESS, { cssClass: 'alert-success', timeout: 5000 });
            _this.ngOnInit();
        }, function (err) {
            _this._flashMessagesService.show(Resource.ERROR, { cssClass: 'alert-danger', timeout: 5000 });
        });
    };
    PodgrupeAddComponent = __decorate([
        Component({
            selector: 'add-podgrupe',
            templateUrl: './add.component.html',
        }),
        __metadata("design:paramtypes", [FormBuilder, RestService, FlashMessagesService,
            DataService])
    ], PodgrupeAddComponent);
    return PodgrupeAddComponent;
}());
export { PodgrupeAddComponent };
//# sourceMappingURL=add.component.js.map