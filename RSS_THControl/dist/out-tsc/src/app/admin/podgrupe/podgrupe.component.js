var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { DataService } from '../../shared/services/data.service';
import { Router } from '../../../../node_modules/@angular/router';
var PodgrupeComponent = /** @class */ (function () {
    function PodgrupeComponent(_service, router) {
        this._service = _service;
        this.router = router;
        this.p = 1;
    }
    PodgrupeComponent.prototype.ngOnInit = function () {
        this._service.getPodgrupe();
    };
    PodgrupeComponent.prototype.edit = function (podgrupa) {
        this._service.selectedPodgrupa = podgrupa;
        this.router.navigate(['admin/podgrupe/uredi', this._service.selectedPodgrupa.id]);
    };
    PodgrupeComponent = __decorate([
        Component({
            selector: 'podgrupe',
            templateUrl: './podgrupe.component.html'
        }),
        __metadata("design:paramtypes", [DataService, Router])
    ], PodgrupeComponent);
    return PodgrupeComponent;
}());
export { PodgrupeComponent };
//# sourceMappingURL=podgrupe.component.js.map