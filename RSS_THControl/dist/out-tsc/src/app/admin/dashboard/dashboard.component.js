var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component, ChangeDetectorRef } from '@angular/core';
import { DataService } from '../../shared/services/data.service';
import { RestService } from '../../shared/services/rest.service';
import { FormBuilder, Validators } from '../../../../node_modules/@angular/forms';
var AdminDashboardComponent = /** @class */ (function () {
    function AdminDashboardComponent(restService, changeRef, _service, formBuilder, _restService) {
        this.restService = restService;
        this.changeRef = changeRef;
        this._service = _service;
        this.formBuilder = formBuilder;
        this._restService = _restService;
        this.p = 1;
        this.p1 = 1;
        this.o1 = 1;
        this.showXAxis1 = true;
        this.showYAxis1 = true;
        this.gradient1 = false;
        this.showLegend1 = true;
        this.showXAxisLabel1 = false;
        this.xAxisLabel1 = '';
        this.showYAxisLabel1 = true;
        this.yAxisLabel1 = '';
        this.timeline1 = false;
        this.legendTitle1 = "";
        this.view = [500, 300];
        this.isChart = false;
        this.multi1 = [
            {
                name: 'Cyan',
                series: [
                    {
                        name: 5,
                        value: 2650
                    },
                    {
                        name: 10,
                        value: 2800
                    },
                    {
                        name: 15,
                        value: 2000
                    }
                ]
            },
            {
                name: 'Yellow',
                series: [
                    {
                        name: 5,
                        value: 2500
                    },
                    {
                        name: 9,
                        value: 5100
                    },
                    {
                        name: 15,
                        value: 2350
                    }
                ]
            }
        ];
        this.colorScheme1 = {
            domain: ['#6e339c', '#a099d5', '#75f65b', '#e9668b', '#ddf731', '#bcac18', '#3c4a74', '#2646f1', '#d62c73', '#f23d65', '#6e339c', '#6e339c', '#abaa9b', '#bcac18', '#579305']
        };
    }
    Object.defineProperty(AdminDashboardComponent.prototype, "f", {
        get: function () { return this.filterForm.controls; },
        enumerable: true,
        configurable: true
    });
    AdminDashboardComponent.prototype.openChart = function (uredjajId) {
        var _this = this;
        this.restService.getDetaljiLogera(uredjajId).subscribe(function (data) {
            _this.multi1 = data;
            _this.isChart = true;
            _this.changeRef.detectChanges();
        });
    };
    AdminDashboardComponent.prototype.ngOnInit = function () {
        var _this = this;
        this._service.getTrenutnoStanje();
        this._service.checkMjerenja(0, 0);
        this._service.getAllCritical();
        this.filterForm = this.formBuilder.group({
            grupaId: [0, Validators.required],
            podgrupaId: [0, Validators.required],
        });
        this._restService.getGrupe(this._service.klijentId).subscribe(function (data) {
            _this.grupe = data;
        });
    };
    AdminDashboardComponent.prototype.onChange = function () {
        var grupaId = this.f.grupaId.value;
        this.selectedGrupa = grupaId;
        this._service.getPodgrupeByGroup(grupaId);
    };
    AdminDashboardComponent.prototype.pretrazi = function () {
        var grupaId = this.f.grupaId.value;
        var podgrupaId = this.f.podgrupaId.value;
        if (grupaId == undefined)
            grupaId = 0;
        if (podgrupaId == undefined)
            podgrupaId = 0;
        if (grupaId == 0)
            podgrupaId = 0;
        this._service.checkMjerenja(grupaId, podgrupaId);
    };
    AdminDashboardComponent = __decorate([
        Component({
            selector: 'admin-dashboard',
            templateUrl: './dashboard.component.html',
        }),
        __metadata("design:paramtypes", [RestService, ChangeDetectorRef, DataService,
            FormBuilder, RestService])
    ], AdminDashboardComponent);
    return AdminDashboardComponent;
}());
export { AdminDashboardComponent };
//# sourceMappingURL=dashboard.component.js.map