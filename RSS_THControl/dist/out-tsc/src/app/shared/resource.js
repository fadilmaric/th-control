var Resource = /** @class */ (function () {
    function Resource() {
    }
    Object.defineProperty(Resource, "ERROR", {
        get: function () { return "Dogodila se greška. Pokušajte ponovno!"; },
        enumerable: true,
        configurable: true
    });
    ;
    Object.defineProperty(Resource, "UPDATE_SUCCESS", {
        get: function () { return "Uspješno ste ažurirali podatke!"; },
        enumerable: true,
        configurable: true
    });
    ;
    Object.defineProperty(Resource, "ADD_SUCCESS", {
        get: function () { return "Uspješno ste dodali podatak!"; },
        enumerable: true,
        configurable: true
    });
    ;
    Object.defineProperty(Resource, "DELETE_SUCCESS", {
        get: function () { return "Uspješno ste obrisali podatak!"; },
        enumerable: true,
        configurable: true
    });
    ;
    Object.defineProperty(Resource, "STATUS_SUCCESS", {
        get: function () { return "Uspješno ste promijenili status!"; },
        enumerable: true,
        configurable: true
    });
    ;
    Object.defineProperty(Resource, "LOGIN_ERROR", {
        get: function () { return "Niste unijeli ispravne korisničke podatke!"; },
        enumerable: true,
        configurable: true
    });
    ;
    Object.defineProperty(Resource, "USERNAME_ERROR", {
        get: function () { return "Korisničko ime je zauzeto!"; },
        enumerable: true,
        configurable: true
    });
    ;
    return Resource;
}());
export { Resource };
//# sourceMappingURL=resource.js.map