var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Injectable } from "@angular/core";
import { Router } from "../../../../node_modules/@angular/router";
import { RestService } from "./rest.service";
var DataService = /** @class */ (function () {
    function DataService(router, _service) {
        this.router = router;
        this._service = _service;
        this.colors = ["#716aca", "#36a3f7", "#f4516c", "#34bfa3", "B2B1CF", "#F1BC2F"];
        this.filteredPodgrupe = [];
    }
    DataService.prototype.authorization = function () {
        this.userRole = parseInt(sessionStorage.getItem("Role"));
        this.klijentId = parseInt(sessionStorage.getItem("KlijentId"));
        this.username = sessionStorage.getItem("Username");
        this.imePrezime = sessionStorage.getItem("ImePrezime");
        this.korisnikId = parseInt(sessionStorage.getItem("KorisnikId"));
        if (this.userRole == null) {
            this.router.navigate(["/prijava"]);
        }
        else if (this.userRole == 3) {
            this.router.navigate(["/rss"]);
        }
        else if (this.userRole == 1) {
            this.router.navigate(["/admin"]);
        }
        else if (this.userRole == 2) {
            this.router.navigate(["/zaposlenik"]);
        }
    };
    DataService.prototype.getKlijenti = function () {
        var _this = this;
        this._service.getKlijenti().subscribe(function (data) {
            _this.klijenti = data;
        });
    };
    DataService.prototype.getKorisnike = function () {
        var _this = this;
        if (this.klijentId == undefined || this.klijentId == null)
            this.klijentId = 0;
        this._service.getKorisnike(this.userRole, this.klijentId).subscribe(function (data) {
            _this.korisnici = data;
        });
    };
    DataService.prototype.getZaposlenike = function () {
        var _this = this;
        if (this.klijentId == undefined || this.klijentId == null)
            this.klijentId = 0;
        this._service.getKorisnike(this.userRole, this.klijentId).subscribe(function (data) {
            _this.zaposlenici = data;
        });
    };
    DataService.prototype.getUredjaje = function (grupaId, podgrupaId) {
        var _this = this;
        if (this.klijentId == undefined || this.klijentId == null)
            this.klijentId = 0;
        this._service.getUredjaje(this.klijentId, grupaId, podgrupaId).subscribe(function (data) {
            _this.uredjaji = data;
        });
    };
    DataService.prototype.getUredjajeByKorisnik = function (korisnikId) {
        var _this = this;
        this._service.getUredjajeByKorisnik(korisnikId).subscribe(function (data) {
            _this.uredjaji = data;
        });
    };
    DataService.prototype.getGrupe = function () {
        var _this = this;
        if (this.klijentId == undefined || this.klijentId == null)
            this.klijentId = 0;
        this._service.getGrupe(this.klijentId).subscribe(function (data) {
            _this.grupe = data;
        });
    };
    DataService.prototype.getPodgrupe = function () {
        var _this = this;
        if (this.klijentId == undefined || this.klijentId == null)
            this.klijentId = 0;
        this._service.getPodgrupe(this.klijentId).subscribe(function (data) {
            _this.podgrupe = data;
        });
    };
    DataService.prototype.getPodgrupeByGroup = function (groupId) {
        var _this = this;
        this.filteredPodgrupe = [];
        if (this.klijentId == undefined || this.klijentId == null)
            this.klijentId = 0;
        this._service.getPodgrupe(this.klijentId).subscribe(function (data) {
            _this.podgrupe = data;
            for (var i = 0; i < _this.podgrupe.length; i++) {
                if (_this.podgrupe[i].grupaId == groupId)
                    _this.filteredPodgrupe.push(_this.podgrupe[i]);
            }
        });
    };
    DataService.prototype.getBrojLogera = function () {
        var _this = this;
        this._service.getCountUredjaja().subscribe(function (data) {
            _this.brojLogera = data;
            var n = 0;
            for (var i = 0; i < _this.brojLogera.length; i++) {
                if (n < 6) {
                    _this.brojLogera[i].color = _this.colors[n];
                    n++;
                }
                else
                    n = 0;
            }
        });
    };
    DataService.prototype.getTrenutnoStanje = function () {
        var _this = this;
        this._service.getTrenutnoStanje(this.klijentId).subscribe(function (data) {
            _this.trenutnoStanje = data;
        });
    };
    DataService.prototype.getTrenutnoStanjeKorisnik = function () {
        var _this = this;
        this._service.getTrenutnoStanjeKorisnik(this.korisnikId).subscribe(function (data) {
            _this.trenutnoStanjeKorisnik = data;
        });
    };
    DataService.prototype.checkMjerenja = function (grupaId, podgrupaId) {
        var _this = this;
        if (this.klijentId == undefined || this.klijentId == null)
            this.klijentId = 0;
        this._service.checkMjerenja(this.klijentId, grupaId, podgrupaId).subscribe(function (data) {
            _this.uredjajiValid = data;
        });
    };
    DataService.prototype.getAllCritical = function () {
        var _this = this;
        this._service.getAllCritical().subscribe(function (data) {
            _this.criticalMjerenja = data;
            console.log(_this.criticalMjerenja);
        });
    };
    DataService.prototype.logout = function () {
        sessionStorage.removeItem("Role");
        sessionStorage.removeItem("Username");
        this.router.navigate(["/prijava"]);
    };
    DataService = __decorate([
        Injectable(),
        __metadata("design:paramtypes", [Router, RestService])
    ], DataService);
    return DataService;
}());
export { DataService };
//# sourceMappingURL=data.service.js.map