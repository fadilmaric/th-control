var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Injectable } from "@angular/core";
import { HttpClient } from "../../../../node_modules/@angular/common/http";
var RestService = /** @class */ (function () {
    function RestService(http) {
        this.http = http;
        this.url_path = "http://th-control.ml/api/";
    }
    RestService.prototype.getKlijenti = function () {
        return this.http.get(this.url_path + "Klijenti/GetAll");
    };
    RestService.prototype.getKorisnike = function (ulogaid, klijentId) {
        return this.http.get(this.url_path + "Korisnici/GetAll?ulogaID=" + ulogaid + "&klijentID=" + klijentId);
    };
    RestService.prototype.getUredjaje = function (klijentId, grupaId, podgrupaId) {
        return this.http.get(this.url_path + "Logeri/GetAll?klijentID=" + klijentId + "&grupaID=" + grupaId + "&podgrupaId=" + podgrupaId);
    };
    RestService.prototype.getUredjajeByKorisnik = function (korisnikId) {
        return this.http.get(this.url_path + "Logeri/GetAllByUser?korisnikID=" + korisnikId);
    };
    RestService.prototype.getMjerenja = function (startDate, endDate, logerId, grupaId, podgrupaId) {
        return this.http.get(this.url_path + "Mjerenja/GetAll?datumOd=" + startDate + "&datumDo=" + endDate + "&logerID=" + logerId +
            "&grupaID=" + grupaId + "&podgrupaId=" + podgrupaId);
    };
    RestService.prototype.getDetaljiLogera = function (uredjajId) {
        return this.http.get(this.url_path + "Mjerenja/GetDetaljeZaLoger?logerID=" + uredjajId);
    };
    RestService.prototype.getGrupe = function (klijentId) {
        return this.http.get(this.url_path + "Grupe/GetAll?klijentID=" + klijentId);
    };
    RestService.prototype.getPodgrupe = function (klijentId) {
        return this.http.get(this.url_path + "Podgrupe/GetAll?klijentID=" + klijentId);
    };
    RestService.prototype.getCountUredjaja = function () {
        return this.http.get(this.url_path + "Klijenti/CountLogeraPoKlijentu");
    };
    RestService.prototype.getAllCritical = function () {
        return this.http.get(this.url_path + "Mjerenja/getallcritical");
    };
    RestService.prototype.getTrenutnoStanje = function (klijentId) {
        return this.http.get(this.url_path + "Logeri/trenutnostanje?klijentID=" + klijentId);
    };
    RestService.prototype.getTrenutnoStanjeKorisnik = function (korisnikId) {
        return this.http.get(this.url_path + "Logeri/TrenutnoStanjeZaKorisnika?korisnikID=" + korisnikId);
    };
    RestService.prototype.checkMjerenja = function (klijentID, grupaId, podgrupaId) {
        return this.http.get(this.url_path + "Logeri/provjeramjerenja?klijentID=" + klijentID + "&grupaId=" + grupaId + "&podgrupaId=" + podgrupaId);
    };
    RestService.prototype.addKlijent = function (klijent) {
        return this.http.post(this.url_path + "Klijenti/Insert", klijent);
    };
    RestService.prototype.addKorisnik = function (korisnik) {
        return this.http.post(this.url_path + "Korisnici/Insert", korisnik);
    };
    RestService.prototype.addUredjaj = function (uredjaj) {
        return this.http.post(this.url_path + "Logeri/Insert", uredjaj);
    };
    RestService.prototype.addGrupa = function (grupa) {
        return this.http.post(this.url_path + "Grupe/Insert", grupa);
    };
    RestService.prototype.addPodgrupa = function (grupa) {
        return this.http.post(this.url_path + "Podgrupe/Insert", grupa);
    };
    RestService.prototype.updateKlijent = function (klijent) {
        return this.http.post(this.url_path + "Klijenti/Edit", klijent);
    };
    RestService.prototype.updateKorisnik = function (korisnik) {
        return this.http.post(this.url_path + "Korisnici/Edit", korisnik);
    };
    RestService.prototype.updateUredjaj = function (uredjaj) {
        return this.http.post(this.url_path + "Logeri/Edit", uredjaj);
    };
    RestService.prototype.updateGrupa = function (grupa) {
        return this.http.post(this.url_path + "Grupe/Edit", grupa);
    };
    RestService.prototype.updatePodgrupa = function (podgrupa) {
        return this.http.post(this.url_path + "Podgrupe/Edit", podgrupa);
    };
    RestService.prototype.updateKorisnikeLogere = function (uredjaj) {
        return this.http.post(this.url_path + "Logeri/EditLogeriKorisnici", uredjaj);
    };
    RestService.prototype.changeKlijentStatus = function (klijent) {
        return this.http.post(this.url_path + "Klijenti/ChangeStatus", klijent);
    };
    RestService.prototype.changeKorisnikStatus = function (korisnik) {
        return this.http.post(this.url_path + "Korisnici/ChangeStatus", korisnik);
    };
    RestService.prototype.login = function (username, password) {
        return this.http.get(this.url_path + "Korisnici/Login?username=" + username + "&password=" + password);
    };
    RestService = __decorate([
        Injectable(),
        __metadata("design:paramtypes", [HttpClient])
    ], RestService);
    return RestService;
}());
export { RestService };
//# sourceMappingURL=rest.service.js.map