var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { Pipe } from '@angular/core';
import { DatePipe } from '@angular/common';
var dateFormatPipe = /** @class */ (function () {
    function dateFormatPipe() {
    }
    dateFormatPipe.prototype.transform = function (value) {
        var datePipe = new DatePipe("en-US");
        value = datePipe.transform(value, 'dd.MM.yyyy HH:mm:ss');
        return value;
    };
    dateFormatPipe = __decorate([
        Pipe({
            name: 'dateFormatPipe',
        })
    ], dateFormatPipe);
    return dateFormatPipe;
}());
export { dateFormatPipe };
//# sourceMappingURL=date.pipe.js.map