var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component, ChangeDetectorRef } from '@angular/core';
import { DataService } from '../../shared/services/data.service';
import { Korisnici } from '../../shared/models/korisnici.model';
import { Resource } from '../../shared/resource';
import { Validators, FormBuilder } from '../../../../node_modules/@angular/forms';
import { FlashMessagesService } from '../../../../node_modules/angular2-flash-messages';
import { RestService } from '../../shared/services/rest.service';
var ProfilZaposlenikComponent = /** @class */ (function () {
    function ProfilZaposlenikComponent(_flashMessagesService, formBuilder, _service, _dataService, _changeRef) {
        this._flashMessagesService = _flashMessagesService;
        this.formBuilder = formBuilder;
        this._service = _service;
        this._dataService = _dataService;
        this._changeRef = _changeRef;
        this.submitted = false;
    }
    Object.defineProperty(ProfilZaposlenikComponent.prototype, "f", {
        get: function () { return this.korisnikForm.controls; },
        enumerable: true,
        configurable: true
    });
    ProfilZaposlenikComponent.prototype.ngOnInit = function () {
        this.korisnikForm = this.formBuilder.group({
            imePrezime: [this._dataService.imePrezime, Validators.required],
            korisnickoIme: [this._dataService.username, Validators.required],
            lozinka: [''],
        });
    };
    ProfilZaposlenikComponent.prototype.submit = function () {
        var _this = this;
        if (this.korisnikForm.invalid) {
            this.submitted = true;
            return;
        }
        this.korisnik = new Korisnici();
        this.korisnik.id = this._dataService.korisnikId;
        this.korisnik.imePrezime = this.f.imePrezime.value;
        this.korisnik.ime = this.f.korisnickoIme.value;
        this.korisnik.firma = this._dataService.klijentId;
        this.korisnik.lozinka = this.f.lozinka.value;
        this.korisnik.ulogaID = 2;
        this._dataService.username = this.f.korisnickoIme.value;
        this._dataService.imePrezime = this.f.imePrezime.value;
        this._changeRef.detectChanges();
        this._service.updateKorisnik(this.korisnik).subscribe(function (data) {
            if (data == true) {
                _this._flashMessagesService.show(Resource.UPDATE_SUCCESS, { cssClass: 'alert-success', timeout: 5000 });
            }
            else {
                _this._flashMessagesService.show(Resource.USERNAME_ERROR, { cssClass: 'alert-danger', timeout: 5000 });
            }
        }, function (err) {
            _this._flashMessagesService.show(Resource.ERROR, { cssClass: 'alert-danger', timeout: 5000 });
        });
    };
    ProfilZaposlenikComponent = __decorate([
        Component({
            selector: 'zaposlenik-profil',
            templateUrl: './profil.component.html'
        }),
        __metadata("design:paramtypes", [FlashMessagesService, FormBuilder, RestService,
            DataService, ChangeDetectorRef])
    ], ProfilZaposlenikComponent);
    return ProfilZaposlenikComponent;
}());
export { ProfilZaposlenikComponent };
//# sourceMappingURL=profil.component.js.map