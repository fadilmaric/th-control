var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { DataService } from '../../shared/services/data.service';
import { Validators, FormBuilder } from '../../../../node_modules/@angular/forms';
import { FlashMessagesService } from '../../../../node_modules/angular2-flash-messages';
import { Router } from '../../../../node_modules/@angular/router';
import { NgxSpinnerService } from '../../../../node_modules/ngx-spinner';
import { RestService } from '../../shared/services/rest.service';
var MjerenjaEmployeeComponent = /** @class */ (function () {
    function MjerenjaEmployeeComponent(_service, router, _restService, _flashMessagesService, spinner, formBuilder) {
        this._service = _service;
        this.router = router;
        this._restService = _restService;
        this._flashMessagesService = _flashMessagesService;
        this.spinner = spinner;
        this.formBuilder = formBuilder;
        this.p = 1;
    }
    Object.defineProperty(MjerenjaEmployeeComponent.prototype, "f", {
        get: function () { return this.filterForm.controls; },
        enumerable: true,
        configurable: true
    });
    MjerenjaEmployeeComponent.prototype.ngOnInit = function () {
        var _this = this;
        this._service.mjerenja = [];
        var start = new Date();
        this.startDate = (start.getMonth() + 1) + '/' + start.getDate() + '/' + start.getFullYear();
        this.endDate = (start.getMonth() + 1) + '/' + start.getDate() + '/' + start.getFullYear();
        this.selectedKlijent = -1;
        $(document).ready(function () {
            $("#m_datepicker_2").datepicker({ todayHighlight: !0, orientation: "bottom left" });
            $("#m_datepicker_3").datepicker({ todayHighlight: !0, orientation: "bottom left" });
        });
        this.filterForm = this.formBuilder.group({
            startDate: [this.startDate, Validators.required],
            endDate: [this.endDate, Validators.required],
            uredjajId: [-1, Validators.required],
        });
        if (this._service.klijentId == undefined || this._service.klijentId == null)
            this._service.klijentId = 0;
        this._restService.getUredjajeByKorisnik(this._service.korisnikId).subscribe(function (data) {
            _this.uredjaji = data;
        });
    };
    MjerenjaEmployeeComponent.prototype.pretrazi = function (startDate, endDate) {
        var _this = this;
        var sDate = startDate.value;
        var eDate = endDate.value;
        var uredjajId = this.f.uredjajId.value;
        var grupaId = 0;
        var podgrupaId = 0;
        if (sDate == "" && eDate == "") {
            this.startDate = this.startDate;
            this.endDate = this.endDate;
        }
        if (sDate != "" || eDate != "") {
            if (sDate != "")
                this.startDate = sDate;
            if (eDate != "")
                this.endDate = eDate;
            if (sDate == "")
                this.startDate = this.endDate;
            else if (eDate == "")
                this.endDate = this.startDate;
        }
        if (uredjajId == -1)
            uredjajId = 0;
        this.spinner.show();
        this._restService.getMjerenja(this.startDate, this.endDate, uredjajId, grupaId, podgrupaId).subscribe(function (data) {
            _this._service.mjerenja = data;
            _this.spinner.hide();
        });
    };
    MjerenjaEmployeeComponent = __decorate([
        Component({
            selector: 'zaposlenik-mjerenja',
            templateUrl: './mjerenja.component.html'
        }),
        __metadata("design:paramtypes", [DataService, Router, RestService,
            FlashMessagesService, NgxSpinnerService,
            FormBuilder])
    ], MjerenjaEmployeeComponent);
    return MjerenjaEmployeeComponent;
}());
export { MjerenjaEmployeeComponent };
//# sourceMappingURL=mjerenja.component.js.map