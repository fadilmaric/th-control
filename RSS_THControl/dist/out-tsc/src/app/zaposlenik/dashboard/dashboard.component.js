var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component, ChangeDetectorRef } from '@angular/core';
import { DataService } from '../../shared/services/data.service';
import { RestService } from '../../shared/services/rest.service';
var ZaposlenikDashboardComponent = /** @class */ (function () {
    function ZaposlenikDashboardComponent(_service, restService, changeRef) {
        this._service = _service;
        this.restService = restService;
        this.changeRef = changeRef;
        this.showXAxis1 = true;
        this.showYAxis1 = true;
        this.gradient1 = false;
        this.showLegend1 = true;
        this.showXAxisLabel1 = false;
        this.xAxisLabel1 = '';
        this.showYAxisLabel1 = true;
        this.yAxisLabel1 = '';
        this.timeline1 = false;
        this.legendTitle1 = "";
        this.view = [500, 300];
        this.isChart = false;
        this.multi1 = [
            {
                name: 'Cyan',
                series: [
                    {
                        name: 5,
                        value: 2650
                    },
                    {
                        name: 10,
                        value: 2800
                    },
                    {
                        name: 15,
                        value: 2000
                    }
                ]
            },
            {
                name: 'Yellow',
                series: [
                    {
                        name: 5,
                        value: 2500
                    },
                    {
                        name: 9,
                        value: 5100
                    },
                    {
                        name: 15,
                        value: 2350
                    }
                ]
            }
        ];
        this.colorScheme1 = {
            domain: ['#6e339c', '#a099d5', '#75f65b', '#e9668b', '#ddf731', '#bcac18', '#3c4a74', '#2646f1', '#d62c73', '#f23d65', '#6e339c', '#6e339c', '#abaa9b', '#bcac18', '#579305']
        };
    }
    ZaposlenikDashboardComponent.prototype.ngOnInit = function () {
        this._service.getTrenutnoStanjeKorisnik();
    };
    ZaposlenikDashboardComponent.prototype.openChart = function (uredjajId) {
        var _this = this;
        this.restService.getDetaljiLogera(uredjajId).subscribe(function (data) {
            _this.multi1 = data;
            _this.isChart = true;
            _this.changeRef.detectChanges();
        });
    };
    ZaposlenikDashboardComponent = __decorate([
        Component({
            selector: 'zaposlenik-dashboard',
            templateUrl: './dashboard.component.html',
        }),
        __metadata("design:paramtypes", [DataService, RestService, ChangeDetectorRef])
    ], ZaposlenikDashboardComponent);
    return ZaposlenikDashboardComponent;
}());
export { ZaposlenikDashboardComponent };
//# sourceMappingURL=dashboard.component.js.map