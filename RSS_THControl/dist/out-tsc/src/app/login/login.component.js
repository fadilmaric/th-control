var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { FormBuilder, Validators } from '../../../node_modules/@angular/forms';
import { RestService } from '../shared/services/rest.service';
import { FlashMessagesService } from '../../../node_modules/angular2-flash-messages/module/flash-messages.service';
import { Resource } from '../shared/resource';
import { DataService } from '../shared/services/data.service';
var LoginComponent = /** @class */ (function () {
    function LoginComponent(formBuilder, _service, _flashMessagesService, _dataService) {
        this.formBuilder = formBuilder;
        this._service = _service;
        this._flashMessagesService = _flashMessagesService;
        this._dataService = _dataService;
        this.submitted = false;
    }
    Object.defineProperty(LoginComponent.prototype, "f", {
        get: function () { return this.loginForm.controls; },
        enumerable: true,
        configurable: true
    });
    LoginComponent.prototype.ngOnInit = function () {
        sessionStorage.removeItem("KlijentId");
        this._dataService.klijentId = 0;
        this.loginForm = this.formBuilder.group({
            username: ['', Validators.required],
            password: ['', Validators.required],
        });
    };
    LoginComponent.prototype.login = function () {
        var _this = this;
        if (this.loginForm.invalid) {
            this.submitted = true;
            return;
        }
        var username = this.f.username.value;
        var password = this.f.password.value;
        this._service.login(username, password).subscribe(function (data) {
            if (data == null) {
                _this._flashMessagesService.show(Resource.LOGIN_ERROR, { cssClass: 'alert-danger', timeout: 5000 });
            }
            else {
                _this._dataService.loggedUser = data;
                sessionStorage.setItem("Username", data.ime);
                sessionStorage.setItem("Role", data.ulogaID);
                sessionStorage.setItem("KlijentId", data.firma);
                sessionStorage.setItem("ImePrezime", data.imePrezime);
                sessionStorage.setItem("KorisnikId", data.id);
                _this._dataService.authorization();
            }
        }, function (err) {
            _this._flashMessagesService.show(Resource.LOGIN_ERROR, { cssClass: 'alert-danger', timeout: 5000 });
        });
    };
    LoginComponent = __decorate([
        Component({
            selector: 'login',
            templateUrl: './login.component.html',
        }),
        __metadata("design:paramtypes", [FormBuilder, RestService, FlashMessagesService,
            DataService])
    ], LoginComponent);
    return LoginComponent;
}());
export { LoginComponent };
//# sourceMappingURL=login.component.js.map