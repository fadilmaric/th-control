var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { Klijent } from '../../../shared/models/klijent.model';
import { FormBuilder, Validators } from '../../../../../node_modules/@angular/forms';
import { RestService } from '../../../shared/services/rest.service';
import { FlashMessagesService } from '../../../../../node_modules/angular2-flash-messages';
import { Resource } from '../../../shared/resource';
var KlijentAddComponent = /** @class */ (function () {
    function KlijentAddComponent(formBuilder, _service, _flashMessagesService) {
        this.formBuilder = formBuilder;
        this._service = _service;
        this._flashMessagesService = _flashMessagesService;
        this.submitted = false;
    }
    Object.defineProperty(KlijentAddComponent.prototype, "f", {
        get: function () { return this.klijentForm.controls; },
        enumerable: true,
        configurable: true
    });
    KlijentAddComponent.prototype.ngOnInit = function () {
        this.klijentForm = this.formBuilder.group({
            naziv: ['', Validators.required],
        });
    };
    KlijentAddComponent.prototype.submit = function () {
        var _this = this;
        if (this.klijentForm.invalid) {
            this.submitted = true;
            return;
        }
        this.klijent = new Klijent();
        this.klijent.naziv = this.f.naziv.value;
        this.klijent.active = true;
        this._service.addKlijent(this.klijent).subscribe(function (data) {
            _this._flashMessagesService.show(Resource.ADD_SUCCESS, { cssClass: 'alert-success', timeout: 5000 });
            _this.ngOnInit();
        }, function (err) {
            _this._flashMessagesService.show(Resource.ERROR, { cssClass: 'alert-danger', timeout: 5000 });
        });
    };
    KlijentAddComponent = __decorate([
        Component({
            selector: 'add-klijent',
            templateUrl: './add.component.html',
        }),
        __metadata("design:paramtypes", [FormBuilder, RestService, FlashMessagesService])
    ], KlijentAddComponent);
    return KlijentAddComponent;
}());
export { KlijentAddComponent };
//# sourceMappingURL=add.component.js.map