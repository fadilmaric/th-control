var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { DataService } from '../../shared/services/data.service';
import { Router } from '../../../../node_modules/@angular/router';
import { RestService } from '../../shared/services/rest.service';
import { Resource } from '../../shared/resource';
import { FlashMessagesService } from '../../../../node_modules/angular2-flash-messages';
var KorisniciComponent = /** @class */ (function () {
    function KorisniciComponent(_service, router, _restService, _flashMessagesService) {
        this._service = _service;
        this.router = router;
        this._restService = _restService;
        this._flashMessagesService = _flashMessagesService;
        this.p = 1;
    }
    KorisniciComponent.prototype.ngOnInit = function () {
        this._service.getKorisnike();
    };
    KorisniciComponent.prototype.edit = function (korisnik) {
        this._service.selectedKorisnik = korisnik;
        this.router.navigate(['rss/korisnici/uredi', this._service.selectedKorisnik.id]);
    };
    KorisniciComponent.prototype.changeStatus = function (korisnik) {
        var _this = this;
        this._restService.changeKorisnikStatus(korisnik).subscribe(function (data) {
            _this._flashMessagesService.show(Resource.STATUS_SUCCESS, { cssClass: 'alert-success', timeout: 5000 });
            _this.ngOnInit();
        });
    };
    KorisniciComponent = __decorate([
        Component({
            selector: 'korisnici',
            templateUrl: './korisnici.component.html',
        }),
        __metadata("design:paramtypes", [DataService, Router, RestService,
            FlashMessagesService])
    ], KorisniciComponent);
    return KorisniciComponent;
}());
export { KorisniciComponent };
//# sourceMappingURL=korisnici.component.js.map