var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { FormBuilder, Validators } from '../../../../../node_modules/@angular/forms';
import { FlashMessagesService } from '../../../../../node_modules/angular2-flash-messages';
import { RestService } from '../../../shared/services/rest.service';
import { Uredjaji } from '../../../shared/models/uredjaji.model';
import { Resource } from '../../../shared/resource';
var UredjajAddComponent = /** @class */ (function () {
    function UredjajAddComponent(_flashMessagesService, formBuilder, _service) {
        this._flashMessagesService = _flashMessagesService;
        this.formBuilder = formBuilder;
        this._service = _service;
        this.submitted = false;
    }
    Object.defineProperty(UredjajAddComponent.prototype, "f", {
        get: function () { return this.uredjajForm.controls; },
        enumerable: true,
        configurable: true
    });
    UredjajAddComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.uredjajForm = this.formBuilder.group({
            naziv: ['', Validators.required],
            klijentId: [0, Validators.required],
            email1: ['', Validators.required],
            email2: ['', Validators.required],
            tmin: ['', Validators.required],
            tmax: ['', Validators.required],
            hmin: ['', Validators.required],
            hmax: ['', Validators.required],
        });
        this._service.getKlijenti().subscribe(function (data) {
            _this.klijenti = data;
        });
    };
    UredjajAddComponent.prototype.submit = function () {
        var _this = this;
        if (this.uredjajForm.invalid) {
            this.submitted = true;
            return;
        }
        this.uredjaj = new Uredjaji();
        this.uredjaj.naziv = this.f.naziv.value;
        this.uredjaj.idklijenta = this.f.klijentId.value;
        this.uredjaj.email1 = this.f.email1.value;
        this.uredjaj.email2 = this.f.email2.value;
        this.uredjaj.tmin = parseFloat(this.f.tmin.value);
        this.uredjaj.tmax = parseFloat(this.f.tmax.value);
        this.uredjaj.hmax = parseFloat(this.f.hmax.value);
        this.uredjaj.hmin = parseFloat(this.f.hmin.value);
        this._service.addUredjaj(this.uredjaj).subscribe(function (data) {
            _this._flashMessagesService.show(Resource.ADD_SUCCESS, { cssClass: 'alert-success', timeout: 5000 });
            _this.ngOnInit();
        }, function (err) {
            _this._flashMessagesService.show(Resource.ERROR, { cssClass: 'alert-danger', timeout: 5000 });
        });
    };
    UredjajAddComponent = __decorate([
        Component({
            selector: 'add-uredjaj',
            templateUrl: './add.component.html',
        }),
        __metadata("design:paramtypes", [FlashMessagesService, FormBuilder, RestService])
    ], UredjajAddComponent);
    return UredjajAddComponent;
}());
export { UredjajAddComponent };
//# sourceMappingURL=add.component.js.map