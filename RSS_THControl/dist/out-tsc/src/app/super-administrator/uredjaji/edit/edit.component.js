var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { FormBuilder, Validators } from '../../../../../node_modules/@angular/forms';
import { FlashMessagesService } from '../../../../../node_modules/angular2-flash-messages';
import { RestService } from '../../../shared/services/rest.service';
import { Resource } from '../../../shared/resource';
import { DataService } from '../../../shared/services/data.service';
var UredjajEditComponent = /** @class */ (function () {
    function UredjajEditComponent(_flashMessagesService, formBuilder, _service, _dataService) {
        this._flashMessagesService = _flashMessagesService;
        this.formBuilder = formBuilder;
        this._service = _service;
        this._dataService = _dataService;
        this.submitted = false;
    }
    Object.defineProperty(UredjajEditComponent.prototype, "f", {
        get: function () { return this.uredjajForm.controls; },
        enumerable: true,
        configurable: true
    });
    UredjajEditComponent.prototype.ngOnInit = function () {
        var _this = this;
        console.log(this._dataService.selectedUredjaj);
        this.uredjajForm = this.formBuilder.group({
            naziv: [this._dataService.selectedUredjaj.naziv, Validators.required],
            klijentId: [this._dataService.selectedUredjaj.idklijenta, Validators.required],
            email1: [this._dataService.selectedUredjaj.email1, Validators.required],
            email2: [this._dataService.selectedUredjaj.email2, Validators.required],
            tmin: [this._dataService.selectedUredjaj.tmin, Validators.required],
            tmax: [this._dataService.selectedUredjaj.tmax, Validators.required],
            hmin: [this._dataService.selectedUredjaj.hmin, Validators.required],
            hmax: [this._dataService.selectedUredjaj.hmax, Validators.required],
        });
        this._service.getKlijenti().subscribe(function (data) {
            _this.klijenti = data;
            _this.uredjajForm.controls['klijentId'].setValue(_this._dataService.selectedUredjaj.idklijenta);
        });
    };
    UredjajEditComponent.prototype.submit = function () {
        var _this = this;
        if (this.uredjajForm.invalid) {
            this.submitted = true;
            return;
        }
        this.uredjaj = this._dataService.selectedUredjaj;
        this.uredjaj.naziv = this.f.naziv.value;
        this.uredjaj.idklijenta = this.f.klijentId.value;
        this.uredjaj.email1 = this.f.email1.value;
        this.uredjaj.email2 = this.f.email2.value;
        this.uredjaj.tmin = parseFloat(this.f.tmin.value);
        this.uredjaj.tmax = parseFloat(this.f.tmax.value);
        this.uredjaj.hmax = parseFloat(this.f.hmax.value);
        this.uredjaj.hmin = parseFloat(this.f.hmin.value);
        this._service.updateUredjaj(this.uredjaj).subscribe(function (data) {
            _this._flashMessagesService.show(Resource.UPDATE_SUCCESS, { cssClass: 'alert-success', timeout: 5000 });
        }, function (err) {
            _this._flashMessagesService.show(Resource.ERROR, { cssClass: 'alert-danger', timeout: 5000 });
        });
    };
    UredjajEditComponent = __decorate([
        Component({
            selector: 'edit-uredjaj',
            templateUrl: './edit.component.html',
        }),
        __metadata("design:paramtypes", [FlashMessagesService, FormBuilder, RestService,
            DataService])
    ], UredjajEditComponent);
    return UredjajEditComponent;
}());
export { UredjajEditComponent };
//# sourceMappingURL=edit.component.js.map