var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { DataService } from '../../shared/services/data.service';
var SuperAdminDashboardComponent = /** @class */ (function () {
    function SuperAdminDashboardComponent(_service) {
        this._service = _service;
        this.p = 1;
    }
    SuperAdminDashboardComponent.prototype.ngOnInit = function () {
        this._service.getBrojLogera();
        this._service.checkMjerenja(0, 0);
    };
    SuperAdminDashboardComponent = __decorate([
        Component({
            selector: 'super-dashboard',
            templateUrl: './dashboard.component.html',
        }),
        __metadata("design:paramtypes", [DataService])
    ], SuperAdminDashboardComponent);
    return SuperAdminDashboardComponent;
}());
export { SuperAdminDashboardComponent };
//# sourceMappingURL=dashboard.component.js.map