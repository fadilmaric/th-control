var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SuperAdminComponent } from './super-administrator/super-administrator.component';
import { KlijentComponent } from './super-administrator/klijenti/klijent.component';
import { KlijentAddComponent } from './super-administrator/klijenti/add/add.component';
import { KlijentEditComponent } from './super-administrator/klijenti/edit/edit.component';
import { KorisniciComponent } from './super-administrator/korisnici/korisnici.component';
import { KorisniciAddComponent } from './super-administrator/korisnici/add/add.component';
import { KorisniciEditComponent } from './super-administrator/korisnici/edit/edit.component';
import { LoginComponent } from './login/login.component';
import { AdminComponent } from './admin/admin.component';
import { UredjajComponent } from './super-administrator/uredjaji/uredjaji.component';
import { UredjajAddComponent } from './super-administrator/uredjaji/add/add.component';
import { UredjajEditComponent } from './super-administrator/uredjaji/edit/edit.component';
import { ZaposleniciComponent } from './admin/zaposlenici/zaposlenici.componnt';
import { MjerenjaComponent } from './admin/mjerenja/mjerenja.component';
import { ZaposleniciAddComponent } from './admin/zaposlenici/add/add.component';
import { GrupeComponent } from './admin/grupe/grupe.component';
import { GrupaAddComponent } from './admin/grupe/add/add.component';
import { GrupaEditComponent } from './admin/grupe/edit/edit.component';
import { PodgrupeComponent } from './admin/podgrupe/podgrupe.component';
import { PodgrupeAddComponent } from './admin/podgrupe/add/add.component';
import { PodgrupeEditComponent } from './admin/podgrupe/edit/edit.component';
import { UredjajiComponent } from './admin/uredjaji/uredjaji.component';
import { UredjajiKorisniciEditComponent } from './admin/uredjaji/edit/edit.component';
import { ZaposleniciEditComponent } from './admin/zaposlenici/edit/edit.component';
import { ProfilComponent } from './admin/profil/profil.component';
import { EmployeeComponent } from './zaposlenik/zaposlenik.component';
import { MjerenjaEmployeeComponent } from './zaposlenik/mjerenja/mjerenja.component';
import { ProfilZaposlenikComponent } from './zaposlenik/profil/profil.component';
import { SuperAdminDashboardComponent } from './super-administrator/dashboard/dashboard.component';
import { AdminDashboardComponent } from './admin/dashboard/dashboard.component';
import { ZaposlenikDashboardComponent } from './zaposlenik/dashboard/dashboard.component';
var routes = [
    {
        path: 'rss', component: SuperAdminComponent, children: [
            { path: 'naslovnica', component: SuperAdminDashboardComponent },
            { path: 'klijenti', component: KlijentComponent },
            { path: 'klijenti/dodaj', component: KlijentAddComponent },
            { path: 'klijenti/uredi/:id', component: KlijentEditComponent },
            { path: 'korisnici', component: KorisniciComponent },
            { path: 'korisnici/dodaj', component: KorisniciAddComponent },
            { path: 'korisnici/uredi/:id', component: KorisniciEditComponent },
            { path: 'uredjaji', component: UredjajComponent },
            { path: 'uredjaji/dodaj', component: UredjajAddComponent },
            { path: 'uredjaji/uredi/:id', component: UredjajEditComponent },
            { path: '', redirectTo: 'naslovnica', pathMatch: 'full' }
        ]
    },
    { path: 'admin', component: AdminComponent, children: [
            { path: 'naslovnica', component: AdminDashboardComponent },
            { path: 'zaposlenici', component: ZaposleniciComponent },
            { path: 'zaposlenici/dodaj', component: ZaposleniciAddComponent },
            { path: 'zaposlenici/uredi/:id', component: ZaposleniciEditComponent },
            { path: 'mjerenja', component: MjerenjaComponent },
            { path: 'grupe', component: GrupeComponent },
            { path: 'grupe/dodaj', component: GrupaAddComponent },
            { path: 'grupe/uredi/:id', component: GrupaEditComponent },
            { path: 'podgrupe', component: PodgrupeComponent },
            { path: 'podgrupe/dodaj', component: PodgrupeAddComponent },
            { path: 'podgrupe/uredi/:id', component: PodgrupeEditComponent },
            { path: 'uredjaji', component: UredjajiComponent },
            { path: 'uredjaji/uredi/:id', component: UredjajiKorisniciEditComponent },
            { path: 'profil', component: ProfilComponent },
            { path: '', redirectTo: 'naslovnica', pathMatch: 'full' }
        ] },
    { path: 'zaposlenik', component: EmployeeComponent, children: [
            { path: 'naslovnica', component: ZaposlenikDashboardComponent },
            { path: 'mjerenja', component: MjerenjaEmployeeComponent },
            { path: 'profil', component: ProfilZaposlenikComponent },
            { path: '', redirectTo: 'naslovnica', pathMatch: 'full' }
        ] },
    { path: 'prijava', component: LoginComponent },
    { path: '', redirectTo: '/prijava', pathMatch: 'full' }
];
var AppRoutingModule = /** @class */ (function () {
    function AppRoutingModule() {
    }
    AppRoutingModule = __decorate([
        NgModule({
            imports: [RouterModule.forRoot(routes)],
            exports: [RouterModule]
        })
    ], AppRoutingModule);
    return AppRoutingModule;
}());
export { AppRoutingModule };
//# sourceMappingURL=app-routing.module.js.map