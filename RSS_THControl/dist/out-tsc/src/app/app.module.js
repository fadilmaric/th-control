var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SuperAdminComponent } from './super-administrator/super-administrator.component';
import { KlijentComponent } from './super-administrator/klijenti/klijent.component';
import { KlijentAddComponent } from './super-administrator/klijenti/add/add.component';
import { KlijentEditComponent } from './super-administrator/klijenti/edit/edit.component';
import { KorisniciComponent } from './super-administrator/korisnici/korisnici.component';
import { KorisniciAddComponent } from './super-administrator/korisnici/add/add.component';
import { KorisniciEditComponent } from './super-administrator/korisnici/edit/edit.component';
import { LoginComponent } from './login/login.component';
import { DataService } from './shared/services/data.service';
import { AdminComponent } from './admin/admin.component';
import { UredjajComponent } from './super-administrator/uredjaji/uredjaji.component';
import { UredjajAddComponent } from './super-administrator/uredjaji/add/add.component';
import { RestService } from './shared/services/rest.service';
import { HttpClientModule } from '../../node_modules/@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';
import { FormsModule } from '@angular/forms';
import { FlashMessagesModule } from 'angular2-flash-messages';
import { NgxPaginationModule } from 'ngx-pagination';
import { NgxSpinnerModule } from 'ngx-spinner';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { UredjajEditComponent } from './super-administrator/uredjaji/edit/edit.component';
import { ZaposleniciComponent } from './admin/zaposlenici/zaposlenici.componnt';
import { MjerenjaComponent } from './admin/mjerenja/mjerenja.component';
import { dateFormatPipe } from './shared/pipes/date.pipe';
import { GrupeComponent } from './admin/grupe/grupe.component';
import { PodgrupeComponent } from './admin/podgrupe/podgrupe.component';
import { ZaposleniciAddComponent } from './admin/zaposlenici/add/add.component';
import { GrupaAddComponent } from './admin/grupe/add/add.component';
import { GrupaEditComponent } from './admin/grupe/edit/edit.component';
import { PodgrupeAddComponent } from './admin/podgrupe/add/add.component';
import { PodgrupeEditComponent } from './admin/podgrupe/edit/edit.component';
import { UredjajiComponent } from './admin/uredjaji/uredjaji.component';
import { UredjajiKorisniciEditComponent } from './admin/uredjaji/edit/edit.component';
import { ZaposleniciEditComponent } from './admin/zaposlenici/edit/edit.component';
import { ProfilComponent } from './admin/profil/profil.component';
import { EmployeeComponent } from './zaposlenik/zaposlenik.component';
import { MjerenjaEmployeeComponent } from './zaposlenik/mjerenja/mjerenja.component';
import { ProfilZaposlenikComponent } from './zaposlenik/profil/profil.component';
import { SuperAdminDashboardComponent } from './super-administrator/dashboard/dashboard.component';
import { AdminDashboardComponent } from './admin/dashboard/dashboard.component';
import { ZaposlenikDashboardComponent } from './zaposlenik/dashboard/dashboard.component';
import { RoundPipe } from './shared/pipes/round.pipe';
var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        NgModule({
            declarations: [
                AppComponent,
                SuperAdminComponent,
                KlijentComponent,
                KlijentAddComponent,
                KlijentEditComponent,
                KorisniciComponent,
                KorisniciAddComponent,
                KorisniciEditComponent,
                UredjajComponent,
                UredjajAddComponent,
                UredjajEditComponent,
                SuperAdminDashboardComponent,
                LoginComponent,
                AdminComponent,
                AdminDashboardComponent,
                ZaposleniciComponent,
                ZaposleniciAddComponent,
                ZaposleniciEditComponent,
                MjerenjaComponent,
                GrupeComponent,
                GrupaAddComponent,
                GrupaEditComponent,
                PodgrupeComponent,
                PodgrupeAddComponent,
                PodgrupeEditComponent,
                UredjajiComponent,
                UredjajiKorisniciEditComponent,
                ProfilComponent,
                EmployeeComponent,
                MjerenjaEmployeeComponent,
                ProfilZaposlenikComponent,
                ZaposlenikDashboardComponent,
                dateFormatPipe,
                RoundPipe
            ],
            imports: [
                BrowserModule,
                BrowserAnimationsModule,
                AppRoutingModule,
                HttpClientModule,
                NgxPaginationModule,
                ReactiveFormsModule,
                FormsModule,
                FlashMessagesModule.forRoot(),
                NgxSpinnerModule,
                NgxChartsModule
            ],
            providers: [
                DataService,
                RestService,
            ],
            bootstrap: [AppComponent]
        })
    ], AppModule);
    return AppModule;
}());
export { AppModule };
//# sourceMappingURL=app.module.js.map